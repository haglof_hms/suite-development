#pragma once

#ifndef _SampleSuite_H_
#define _SampleSuite_H_

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

#include "stdafx.h"
#include "ZipArchive.h"
#include <vector>

typedef std::vector<INDEX_TABLE> vecINDEX_TABLE;
typedef std::vector<INFO_TABLE> vecINFO_TABLE;

// Initialize the DLL, register the classes etc
extern "C" void DLL_BUILD InitSuite(CStringArray *,vecINDEX_TABLE &, vecINFO_TABLE &);

// Open a document view
extern "C" void DLL_BUILD OpenSuite(int idx,LPCTSTR func,CWnd *,vecINDEX_TABLE &,int *ret);
extern "C" void DLL_BUILD OpenSuiteEx(_user_msg *msg,CWnd *,vecINDEX_TABLE &,int *ret);

//extern SampleSuite theApp;
extern RLFReader* g_pXML;


// zip
extern CZipArchive* g_pZIP;

class CSampleSuite //: public CWinApp
{
public:
	CSampleSuite();
	~CSampleSuite();
	BOOL CheckLicense(CString csLicModule);

	CArray<PROGRAM, PROGRAM> m_caPrograms;
	CArray<LANGUAGE, LANGUAGE> m_caLanguages;
	CArray<KEY, KEY> m_caKeys;

	CString m_csTemppath;

	CString m_csSelProgramDP;
	int m_nSelProgramDP;
	int m_nSelLangDP;
	CString m_csSelProgramMC;
	int m_nSelProgramMC;
	int m_nSelLangMC;

    int m_nType;	// ugly hack to know what called a document
	CString m_csFilePath;	// ^^
	CString m_csDownloadPath;

	CString m_csWebServer;	// server where the public files are
	CString m_csWebPath;	// webpath
	CString m_csWebLogin;	// weblogin
	CString m_csWebPass;	// webpassword

	CString m_csLocalPath;	// local path

	bool m_bWriteAllowed;	// are we allowed to change anything?

	BOOL m_bNew;
	BOOL m_bLock;
	BOOL m_bClick;	// stupid variable used becuase of a stupid concept
};

extern CSampleSuite theApp;

#endif
