// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500


//#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

//#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
//#endif // _AFX_NO_OLE_SUPPORT

#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT


#include "Resource.h"

// XML handling
#import <msxml3.dll> //named_guids
#include <msxml2.h>

// Xtreeme toolkit
#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

//#define _XTLIB_NOAUTOLINK
#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions

//#define _XTP_STATICLINK
//#include <XTToolkitPro.h>

#define IDC_TABCONTROL						9998
const LPCTSTR PROGRAM_NAME			  		= _T("Development");	// Name of suite/module, used on setting Language filename; 051214 p�d

typedef struct _tagPROGRAM
{
	int nID;			// product-id
	int nProgtype;		// type of program
	CString csName;		// name of the program
	CString csLang;		// languages the program support
	CString csArtNum;	// artikelnummer (navision)

	int nLicense;		// need license? (1 = yes)

	int nKeys;			// num of keys
	int nLangs;			// num of langs
} PROGRAM;

typedef struct _tagLANGUAGE
{
	bool bLocal;		// does the file exist locally?
	CString csLang;		// name of the language
	int nVersion;		// version of the local file (if any)
	int nVersionWeb;	// version of the web file

	CString csVersion;
	CString csVersionWeb;

	CString csPath;		// path to the program
	CString csPathWeb;	// webpath to the program
	CString csDesc;		// description about the program
	CString csDesc2;	// description about the new version
	CString csDate;		// date of package
	bool bVisible;		// program visible or not
} LANGUAGE;

typedef struct _tagKEY
{
	int nKey;		// the key id
	int nSerial;	// serialnumber of hardware
	int nLevel;		// licenselevel
	int nCode;		// code
	CString csComment;	// comment
} KEY;

#include "..\HMSFuncLib\ResLangFileReader.h"
#include "..\HMSFuncLib\pad_hms_miscfunc.h"
