// SettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "SettingsDlg.h"
#include ".\settingsdlg.h"
#include "CustomItems.h"

IMPLEMENT_DYNCREATE(CSettingsFrame, CChildFrameBase)

BEGIN_MESSAGE_MAP(CSettingsFrame, CChildFrameBase)
	//{{AFX_MSG_MAP(CSettingsFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSettingsFrame construction/destruction

//XTPDockingPanePaintTheme CSettingsFrame::m_themeCurrent = xtpPaneThemeOffice;

CSettingsFrame::CSettingsFrame()
{
	m_bOnce = TRUE;
}

CSettingsFrame::~CSettingsFrame()
{
}

BOOL CSettingsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CSettingsFrame diagnostics

#ifdef _DEBUG
void CSettingsFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CSettingsFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSettingsFrame message handlers

int CSettingsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	UpdateWindow();

	return 0;
}

void CSettingsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce)
    {
		m_bOnce = false;

		CString csBuf;
		csBuf.Format(_T("%s\\HMS_Development\\Dialogs\\Settings"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

void CSettingsFrame::OnClose()
{
	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

void CSettingsFrame::OnDestroy()
{
	CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\HMS_Development\\Dialogs\\Settings"), REG_ROOT);
	SavePlacement(this, csBuf);
	m_bOnce = TRUE;
}

void CSettingsFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);
}


// CSettingsDlg

IMPLEMENT_DYNCREATE(CSettingsDlg, CXTResizeFormView)

CSettingsDlg::CSettingsDlg()
	: CXTResizeFormView(CSettingsDlg::IDD)
{
}

CSettingsDlg::~CSettingsDlg()
{
}

void CSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CListCtrlDlg)
	DDX_Control(pDX, IDC_PLACEHOLDER, m_wndPlaceHolder);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSettingsDlg, CXTResizeFormView)
	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnValueChanged)
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
END_MESSAGE_MAP()


// CSettingsDlg diagnostics

#ifdef _DEBUG
void CSettingsDlg::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CSettingsDlg::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

BOOL CSettingsDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CSettingsDlg::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	// get a handle to the document
	pDoc = (CMDISampleSuiteDoc*)GetDocument();

	// get the size of the placeholder, this will be used when creating the grid.
	CRect rc;
	m_wndPlaceHolder.GetWindowRect( &rc );
	ScreenToClient( &rc );

	CString csBuf;

	// create the property grid.
	if ( m_wndPropertyGrid.Create( rc, this, IDC_PROPERTY_GRID ) )
	{
		m_wndPropertyGrid.SetVariableItemsHeight(TRUE);

		// Paths category.
		CXTPPropertyGridItem* pPaths = m_wndPropertyGrid.AddCategory(g_pXML->str(1050));

		m_pItemPathLocal = pPaths->AddChildItem(new CCustomItemFileBox(g_pXML->str(1059), theApp.m_csLocalPath));
		m_pItemPathLocal->SetDescription(g_pXML->str(1060));

		pPaths->Expand();


		// Server category.
		CXTPPropertyGridItem* pServer = m_wndPropertyGrid.AddCategory(g_pXML->str(1056));

		m_pItemServer = pServer->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1051), theApp.m_csWebServer));
		m_pItemServer->SetDescription(g_pXML->str(1052));

		m_pItemPathWeb = pServer->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1053), theApp.m_csWebPath));
		m_pItemPathWeb->SetDescription(g_pXML->str(1054));

		m_pItemLogin = pServer->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1057), theApp.m_csWebLogin));

		m_pItemPass = pServer->AddChildItem(new CXTPPropertyGridItem(g_pXML->str(1058), theApp.m_csWebPass));
		m_pItemPass->SetPasswordMask('*');

		pServer->Expand();
	}

	// resize the parent
//	ResizeParentToFit();

	// set the resize
	SetResize(IDC_PROPERTY_GRID, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);

	// resize the window to match the frame
	RECT rect;
	GetParentFrame()->GetClientRect(&rect);
	OnSize(1, rect.right, rect.bottom);

	UpdateData(FALSE);
}


// CSettingsDlg message handlers

void CSettingsDlg::OnClose()
{
	CXTResizeFormView::OnClose();
}

void CSettingsDlg::OnDestroy()
{
	// local path
	theApp.m_csLocalPath = m_pItemPathLocal->GetValue();
	if(theApp.m_csLocalPath.GetAt(theApp.m_csLocalPath.GetLength()-1) != '\\')
		theApp.m_csLocalPath += _T("\\");

	// server settings
	theApp.m_csWebServer = m_pItemServer->GetValue();
	theApp.m_csWebPath = m_pItemPathWeb->GetValue();
	if(theApp.m_csWebPath.GetAt(theApp.m_csWebPath.GetLength()-1) != '/')
		theApp.m_csWebPath += _T("/");
	theApp.m_csWebLogin = m_pItemLogin->GetValue();
	theApp.m_csWebPass = m_pItemPass->GetValue();


	CXTResizeFormView::OnDestroy();
}

void CSettingsDlg::OnSetFocus(CWnd* pOldWnd)
{
	CXTResizeFormView::OnSetFocus(pOldWnd);

	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
}

LRESULT CSettingsDlg::OnValueChanged(WPARAM wParam, LPARAM lParam)
{
	int nGridAction = (int)wParam;
	CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;
	ASSERT(pItem);

	switch (nGridAction)
	{
		case XTP_PGN_SORTORDER_CHANGED:
		{
		}
		break;

		case XTP_PGN_ITEMVALUE_CHANGED:
		{
		}
		break;

		case XTP_PGN_SELECTION_CHANGED:
		{
		}
		break;
	}

	return FALSE;
}
