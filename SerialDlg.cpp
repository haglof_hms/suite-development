// SerialDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "SerialDlg.h"
#include ".\serialdlg.h"


// CSerialDlg dialog

IMPLEMENT_DYNAMIC(CSerialDlg, CDialog)
CSerialDlg::CSerialDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSerialDlg::IDD, pParent)
	, m_csSerial(_T(""))
	, m_csLevel(_T(""))
{
}

CSerialDlg::~CSerialDlg()
{
}

void CSerialDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_csSerial);
	DDX_Control(pDX, IDC_COMBO1, m_cbLevel);
	DDX_CBString(pDX, IDC_COMBO1, m_csLevel);
}


BEGIN_MESSAGE_MAP(CSerialDlg, CDialog)
END_MESSAGE_MAP()


// CSerialDlg message handlers

BOOL CSerialDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString csBuf;
	for(int nLoop=1; nLoop<11; nLoop++)
	{
		csBuf.Format(_T("%d"), nLoop);
		m_cbLevel.AddString(csBuf);
	}
	m_cbLevel.SetCurSel(0);

	GetDlgItem(IDC_EDIT1)->SetFocus();

	return FALSE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CSerialDlg::OnOK()
{
	UpdateData(TRUE);

	CDialog::OnOK();
}
