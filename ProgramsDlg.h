#pragma once
#include <map>
#include "Resource.h"
#include "afxcmn.h"
#include "xmlParser.h"
#include "w3c.h"
#include "afxwin.h"
#include "SampleSuiteForms.h"
#include "MyLocale.h"

class CMessageRecord;

struct LICENSEDATA
{
	CString serial;
	CString name;
	CString version;
	unsigned int code;
	CString filename;
};

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CProgramsFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CProgramsFrame)

	BOOL m_bOnce;
	BOOL m_bLock;
public:
	CProgramsFrame();

// Attributes
public:
//	CXTPDockingPaneManager m_paneManager;
//	CXTPPropertyGrid m_wndPropertyGrid;

	// Toolbar
	CXTPToolBar m_wndToolBar;
	CStatusBar  m_wndStatusBar;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProgramsFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CProgramsFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	void SetButtonEnabled(UINT nID, BOOL bEnabled);

// Generated message map functions
	//{{AFX_MSG(CProgramsFrame)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
protected:
	LRESULT OnMsgSuite(WPARAM wParm, LPARAM lParm);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	int CreateLockfile();
	int RemoveLockfile();
public:
	afx_msg void OnClose();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
};


class CProgramsDlg : public CXTPReportView
{
protected: // create from serialization only
	CProgramsDlg();
	DECLARE_DYNCREATE(CProgramsDlg)

// Attributes
public:
	CMDISampleSuiteDoc* pDoc;

	CXTPReportSubListControl m_wndSubList;
	CXTPReportFilterEditControl m_wndFilterEdit;

	CImageList m_ilIcons;

	BOOL m_bAutomaticFormating;
	BOOL m_bMultilineSample;

	void LoadReportState();
	void SaveReportState();

//	CFrameWnd* m_pTaskFrame;
//	CFrameWnd* m_pPropertiesFrame;

private:
	int m_nVersionXML;
	CString m_csDateXML;

	CLocale m_cLocale;
	int m_nSelProgram;
	int m_nSelLang;

	void DownloadPackage(CString csSerial, CString csLevel, CMessageRecord* pRecord);
	void GenerateLicenseReports();

	std::map<int, LICENSEDATA> m_licenses;


// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProgramsDlg)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CProgramsDlg();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
public:
	int SaveXML();
	afx_msg void OnNew();
	afx_msg void OnDelete();
	void AddSampleRecords();
protected:
	// XML
	int GetLocalXML();
	int GetWebXML();
	int ParseXML();

	int getProductCode(CString filename);
	bool copyFileDP2(CString dest, CString org, CString filename, int m_nSerial, int m_nCode, int m_nLevel, int m_nProd); 

	int getProductCodeDP3(CString filename);
	bool copyFileDP3(CString dest, CString org, CString filename, int m_nSerial, int m_nCode, int m_nLevel, int m_nProd); 

	//{{AFX_MSG(CProgramsDlg)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	afx_msg void OnBnClickedManual();
	afx_msg void OnBnClickedDownload();
	afx_msg void OnBnClickedLicense();
	afx_msg void OnBnClickedRefresh();
	afx_msg void OnBnClickedSelectFiles();
	afx_msg void OnBnClickedSendToDP2();
	afx_msg void OnBnClickedSendToDP3();

	afx_msg void OnGridHorizontal(UINT);
	afx_msg void OnUpdateGridHorizontal(CCmdUI* pCmdUI);

	afx_msg void OnGridVertical(UINT);
	afx_msg void OnUpdateGridVertical(CCmdUI* pCmdUI);
	afx_msg void OnReportHyperlinkClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);

	afx_msg void OnReportSelChanged(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportItemRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnShowFieldChooser();
	afx_msg void OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportBeginDrag(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportBeforePasteFromText(NMHDR * pNotifyStruct, LRESULT * result);

	DECLARE_MESSAGE_MAP()
public:
};
