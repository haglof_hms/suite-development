// ProgramDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Samplesuite.h"
#include "ProgramDlg.h"
#include "ProgramsDlg.h"
#include "MessageRecord.h"
#include "Usefull.h"
#include ".\programdlg.h"

IMPLEMENT_DYNCREATE(CProgramFrame, CChildFrameBase)

BEGIN_MESSAGE_MAP(CProgramFrame, CChildFrameBase)
	//{{AFX_MSG_MAP(CProgramFrame)
	ON_WM_CREATE()
	ON_WM_MDIACTIVATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMsgSuite)
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProgramFrame construction/destruction

CProgramFrame::CProgramFrame()
{
	m_bOnce = TRUE;
}

CProgramFrame::~CProgramFrame()
{
}

LRESULT CProgramFrame::OnMsgSuite(WPARAM wParm, LPARAM lParm)
{
	// user pushed some buttons in the shell.
	switch(wParm)
	{
		case ID_NEW_ITEM:
		break;

		case ID_OPEN_ITEM:
		break;

		case ID_PREVIEW_ITEM:
		break;

		case ID_SAVE_ITEM:
			((CProgramDlg*)GetActiveView())->OnSave();
		break;

		case ID_DELETE_ITEM:
			((CProgramDlg*)GetActiveView())->OnDelete();
		break;
	};

	return 0;
}

void CProgramFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce)
    {
		m_bOnce = false;

		CString csBuf;
		csBuf.Format(_T("%s\\HMS_Development\\Dialogs\\Program"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

void CProgramFrame::OnClose()
{
	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);

	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

void CProgramFrame::OnDestroy()
{
	CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\HMS_Development\\Dialogs\\Program"), REG_ROOT);
	SavePlacement(this, csBuf);
	m_bOnce = TRUE;
}

BOOL CProgramFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CChildFrameBase::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CProgramFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CChildFrameBase::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
        RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

/////////////////////////////////////////////////////////////////////////////
// CProgramFrame diagnostics

#ifdef _DEBUG
void CProgramFrame::AssertValid() const
{
	CChildFrameBase::AssertValid();
}

void CProgramFrame::Dump(CDumpContext& dc) const
{
	CChildFrameBase::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CProgramFrame message handlers

int CProgramFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CChildFrameBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	UpdateWindow();

	return 0;
}

void CProgramFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);
/*
	CSize sz(0);
	if (m_wndToolBar.GetSafeHwnd())
	{
		RECT rect;
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, LM_HORZDOCK|LM_HORZ|LM_COMMIT);

		m_wndToolBar.MoveWindow(0, 0, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
	}*/
}

void CProgramFrame::SetButtonEnabled(UINT nID, BOOL bEnabled)
{
	CXTPToolBar* pToolBar = &m_wndToolBar;
	CXTPControls* p = pToolBar->GetControls();
	CXTPControl* pCtrl = NULL;

	pCtrl = p->GetAt(nID);
	pCtrl->SetEnabled(bEnabled);
}

// CProgramDlg

#define IDC_REPORT 9998
#define COLUMN_SERIAL 0
#define COLUMN_KEY 1


IMPLEMENT_DYNCREATE(CProgramDlg, CXTResizeFormView)

CProgramDlg::CProgramDlg()
	: CXTResizeFormView(CProgramDlg::IDD)
	, m_csName(_T(""))
	, m_csID(_T(""))
	, m_nType(0)
	, m_csVersion(_T(""))
	, m_nLanguage(0)
	, m_csDescription(_T(""))
	, m_csArtNum(_T(""))
	, m_bNeedLicense(FALSE)
	, m_ctDate(0)
	, m_bInvisible(FALSE)
{
	m_csLang = _T("");
	m_bNew = FALSE;
	m_nSelProgram = -1;
	m_nSelLang = -1;
	m_bDontBother = FALSE;
	m_bIsFocus = FALSE;
}

CProgramDlg::~CProgramDlg()
{
}

void CProgramDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProgramDlg)
	DDX_Text(pDX, IDC_EDIT_NAME, m_csName);
	DDX_Text(pDX, IDC_EDIT_ID, m_csID);
	DDX_CBIndex(pDX, IDC_COMBO_TYPE, m_nType);
	DDX_Text(pDX, IDC_EDIT_VERSION, m_csVersion);
	DDX_CBIndex(pDX, IDC_COMBO_LANGUAGE, m_nLanguage);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_REPORT, m_wndReport);
	DDX_Text(pDX, IDC_EDIT_DESCRIPTION, m_csDescription);
	DDX_Check(pDX, IDC_CHECK_NEEDLICENSE, m_bNeedLicense);
	DDX_Check(pDX, IDC_CHECK_INVISIBLE, m_bInvisible);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER1, m_ctDate);
	DDX_Text(pDX, IDC_EDIT_ARTNUM, m_csArtNum);
	DDX_Control(pDX, IDC_EDIT_ARTNUM, m_edArtNum);
//	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER1, m_csDate);
}

BEGIN_MESSAGE_MAP(CProgramDlg, CXTResizeFormView)
	//{{AFX_MSG_MAP(CProgramDlg)
	//}}AFX_MSG_MAP
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_DROPFILES()
	ON_NOTIFY(XTP_NM_REPORT_INPLACEBUTTONDOWN, IDC_REPORT, OnReportFilebutton)
	ON_NOTIFY(XTP_NM_REPORT_SELCHANGED, IDC_REPORT, OnReportSelChanged)
	ON_NOTIFY(NM_CLICK, IDC_REPORT, OnReportSelChanged)
END_MESSAGE_MAP()


// CProgramDlg diagnostics

#ifdef _DEBUG
void CProgramDlg::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CProgramDlg::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CProgramDlg message handlers
BOOL CProgramDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CProgramDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// create the reportcontrol
	if (!m_wndReport.Create(WS_CHILD | WS_TABSTOP | WS_VISIBLE | WM_VSCROLL, CRect(10, 340, lpCreateStruct->cx - 10, lpCreateStruct->cy - 10), this, IDC_REPORT))
	{
		TRACE(_T("Failed to create reportcontrol"));
		return -1;
	}

	m_wndReport.SetGridStyle(FALSE, xtpReportGridNoLines);
//	m_wndReport.GetPaintManager()->m_columnStyle = xtpReportColumnExplorer;
	m_wndReport.GetPaintManager()->SetColumnStyle(xtpReportColumnExplorer);

	return 0;
}

void CProgramDlg::OnInitialUpdate()
{
	m_csName = _T("");
	m_csID = _T("");
	m_nType = 0;
	m_csVersion = _T("");
	m_nLanguage = 0;
	m_csDescription = _T("");
	m_csArtNum = _T("");
	m_bNeedLicense = FALSE;
	m_ctDate = 0;
	m_bInvisible = FALSE;

	// get a handle to the document
	pDoc = (CMDISampleSuiteDoc*)GetDocument();


	// setup the reportcontrol
	m_wndReport.ModifyStyle(0, WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP);
	CXTPReportColumn* pCol = m_wndReport.AddColumn(new CXTPReportColumn(0, _T("Filename"), 50, TRUE, XTP_REPORT_NOICON, FALSE));
	pCol->AllowRemove(FALSE);
	pCol = m_wndReport.AddColumn(new CXTPReportColumn(1, _T("Type"), 50, TRUE, XTP_REPORT_NOICON, FALSE));
	pCol->AllowRemove(FALSE);


	m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
	m_wndReport.DragAcceptFiles();


	if(pDoc->m_bNew != FALSE)
	{
		m_bNew = TRUE;
	}

	CXTResizeFormView::OnInitialUpdate();

	// limit the description to 512 bytes
	((CEdit*)GetDlgItem(IDC_EDIT_DESCRIPTION))->SetLimitText(512);

	// fill the comboboxes
	// type
	CComboBox* pCombo = (CComboBox*)GetDlgItem(IDC_COMBO_TYPE);
	pCombo->AddString(_T("DigitechPro"));
/*	pCombo->AddString(_T("Mantax computer"));
	pCombo->AddString(_T("PocketPC"));
	pCombo->AddString(_T("WinCE"));
	pCombo->AddString(_T("Windows"));*/

	m_edArtNum.SetEditMask(L"00-000-0000",L"__-___-____");

	// language
	int nLoop=0, nFound = 0;
	CString csBuf;

	pCombo = (CComboBox*)GetDlgItem(IDC_COMBO_LANGUAGE);
	do
	{
		csBuf = m_cLocale.GetLangString(nLoop);
		pCombo->AddString(csBuf);

		if(csBuf == m_csLang) nFound = nLoop;

		nLoop++;
	}
	while(csBuf != _T(""));
	m_nLanguage = nFound;


	// Set the resize
//	SetResize(IDC_EDIT_DESCRIPTION, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);
	SetResize(IDC_STATIC_FILES, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);
	SetResize(IDC_REPORT, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);
	

	// resize the window to match the frame
	RECT rect;
	GetParentFrame()->GetClientRect(&rect);
	OnSize(1, rect.right, rect.bottom);

//	ResizeParentToFit();

	UpdateData(FALSE);

}

void CProgramDlg::OnSetFocus(CWnd* pOldWnd)
{
	CString csBuf;

	CView::OnSetFocus(pOldWnd);

	if(m_bDontBother == TRUE) return;

	//if (!m_bIsFocus)
	//{

		// turn off all imagebuttons in the main window
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
		if(pDoc->m_bLock == FALSE && theApp.m_bWriteAllowed == TRUE)
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, TRUE);
		else
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);

		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);

		// save if there is any changes?


		// fill the dialog
		if(pDoc->m_bNew == TRUE)
		{
			GetDlgItem(IDC_EDIT_NAME)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_ID)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_TYPE)->EnableWindow(TRUE);

			if(pDoc->m_bClick == TRUE)
			{
				m_csName = _T("");
				m_csID = _T("");
				m_csVersion = _T("");
				m_csDescription = _T("");
				m_csLang = _T("");
				m_csArtNum = _T("");
				m_nType = theApp.m_nType;
				m_bInvisible = FALSE;

				m_nSelProgram = -1;
				m_nSelLang = -1;

				m_ctDate = CTime::GetCurrentTime();

				// remove files in the reportcontrol
				m_caFiles.RemoveAll();
				m_wndReport.GetRecords()->RemoveAll();
				m_wndReport.AddRecord(new CKeyRecord(_T(""), 0));
				m_wndReport.Populate();
				m_wndReport.AllowEdit(TRUE);
				m_wndReport.FocusSubItems(TRUE);

				UpdateData(FALSE);

				pDoc->m_bClick = FALSE;
			}
		}
		else
		{
			

			if(pDoc->m_bLock == TRUE || theApp.m_bWriteAllowed == FALSE)
			{
				// Open up ALL controls; 2012-10-24 P�D
				GetDlgItem(IDC_EDIT_NAME)->EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_ID)->EnableWindow(FALSE);
				GetDlgItem(IDC_COMBO_TYPE)->EnableWindow(FALSE);

				GetDlgItem(IDC_EDIT_VERSION)->EnableWindow(FALSE);
				GetDlgItem(IDC_COMBO_LANGUAGE)->EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_DESCRIPTION)->EnableWindow(FALSE);
				GetDlgItem(IDC_CHECK_NEEDLICENSE)->EnableWindow(FALSE);
				GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(FALSE);
				GetDlgItem(IDC_CHECK_INVISIBLE)->EnableWindow(FALSE);
			}
			else
			{
				// Open up ALL controls; 2012-10-24 P�D
				// Disable Name and ID; 2012-11-16 P�D
				GetDlgItem(IDC_EDIT_NAME)->EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_ID)->EnableWindow(FALSE);
				GetDlgItem(IDC_COMBO_TYPE)->EnableWindow(TRUE /*FALSE*/);

				GetDlgItem(IDC_EDIT_VERSION)->EnableWindow(TRUE);
				GetDlgItem(IDC_COMBO_LANGUAGE)->EnableWindow(TRUE);
				GetDlgItem(IDC_EDIT_DESCRIPTION)->EnableWindow(TRUE);
				GetDlgItem(IDC_CHECK_NEEDLICENSE)->EnableWindow(TRUE);
				GetDlgItem(IDC_DATETIMEPICKER1)->EnableWindow(TRUE);
				GetDlgItem(IDC_CHECK_INVISIBLE)->EnableWindow(TRUE);
			}

			int nProg, nLang;
			CString csName;

			if(pDoc->m_nType == 0)	// digitech pro
			{
				nProg = theApp.m_nSelProgramDP;
				nLang = theApp.m_nSelLangDP;
				csName = theApp.m_csSelProgramDP;
			}
			else if(pDoc->m_nType == 1)	// mantax computer
			{
				nProg = theApp.m_nSelProgramMC;
				nLang = theApp.m_nSelLangMC;
				csName = theApp.m_csSelProgramMC;
			}

			// check if we have changed program
			if(m_nSelProgram == nProg && m_nSelLang == nLang) return;
			m_nSelProgram = nProg;
			m_nSelLang = nLang;


			// remove files in the reportcontrol
			m_caFiles.RemoveAll();
			m_wndReport.GetRecords()->RemoveAll();
			m_wndReport.Populate();



			PROGRAM prg;
			KEY key;
			LANGUAGE lng;
			FILES file;

			prg = theApp.m_caPrograms.GetAt(nProg);
			lng = theApp.m_caLanguages.GetAt(nLang);

			int nYear = 0, nMonth = 0, nDay = 0;
			CString sYear = L"", sMonth = L"", sDay = L"";
			m_csName = prg.csName;
			m_csID.Format(_T("%d"), prg.nID);
			m_nType = prg.nProgtype;
			m_csArtNum = prg.csArtNum;
	//		m_csVersion.Format(_T("%d.%d"), lng.nVersion/10, lng.nVersion%10);
			m_csVersion = lng.csVersion;
			m_csDescription = lng.csDesc;
			m_csLang = m_cLocale.GetLangString(lng.csLang);
			m_nType = theApp.m_nType;
			m_csDate = lng.csDate;
			// �ndrat
			AfxExtractSubString(sYear,m_csDate,0,'-');
			AfxExtractSubString(sMonth,m_csDate,1,'-');
			AfxExtractSubString(sDay,m_csDate,2,'-');
			nYear = _tstoi(sYear);
			nMonth = _tstoi(sMonth);
			nDay = _tstoi(sDay);
			// Check that month and day are within limits
			if (nMonth < 1 || nMonth > 12)
				nMonth = 1;
			if (nDay < 1 || nDay > 31)
				nDay = 1;
			m_ctDate = CTime(nYear, nMonth, nDay, 12, 0, 0);


			// Kommenterat bort; ger bug om m�nad �r Oktober = 10
			// Funktionen: _tstoi(lng.csDate.Mid(6, 2)) returnerar 0
			//m_ctDate = CTime(_tstoi(lng.csDate.Left(4)), _tstoi(lng.csDate.Mid(6, 2)), _tstoi(lng.csDate.Right(2)), 12, 0, 0);
			
			
			if(prg.nLicense > 0) m_bNeedLicense = TRUE;
			else m_bNeedLicense = FALSE;
			m_bInvisible = !lng.bVisible;


			// change language selection
			int nLoop=0, nFound = 0;

			CComboBox* pCombo = (CComboBox*)GetDlgItem(IDC_COMBO_LANGUAGE);
			do
			{
				csBuf = m_cLocale.GetLangString(nLoop);
				if(csBuf == m_csLang)
				{
					nFound = nLoop;
					break;
				}

				nLoop++;
			}
			while(csBuf != _T(""));
			m_nLanguage = nFound;



			// get the files from the package
			csBuf = lng.csPath.Right(4);
			if(csBuf.CompareNoCase(_T(".hmi")) == 0)
			{
				m_bNew = FALSE;
				CString csDestpath;
				csDestpath = theApp.m_csLocalPath;

				g_pZIP->Open(csDestpath /*+ _T("\\")*/ + lng.csPath, CZipArchive::zipOpenReadOnly);

				// extract "package.xml" and inspect it
				OpenPackage();

				g_pZIP->Close();
			}

			UpdateData(FALSE);
			
			// always add an empty, final, line
			m_wndReport.AddRecord(new CKeyRecord(_T(""), 0));
			m_wndReport.Populate();
			if(pDoc->m_bLock == TRUE)
				m_wndReport.AllowEdit(FALSE);
			else
				m_wndReport.AllowEdit(TRUE);
			m_wndReport.FocusSubItems(TRUE);
		}

		GetDlgItem(IDC_EDIT_NAME)->SetFocus();


		//m_bIsFocus = TRUE;
	//}

//	UpdateData(FALSE);
}

// save the package
void CProgramDlg::OnSave()
{
	UpdateData(TRUE);

	int nLoop;

	if(m_csID == _T("") || m_csName == _T("") || m_csVersion == _T("") || m_csArtNum == _T("") || m_nLanguage == -1)
	{
		m_bDontBother = TRUE;
		AfxMessageBox(_T("Du har inte fyllt i tillr�ckligt med information!"));
		m_bDontBother = FALSE;
		return;
	}

	if(m_csDescription.Find('<', 0) != -1 || m_csDescription.Find('>', 0) != -1)
	{
		m_bDontBother = TRUE;
		AfxMessageBox(_T("Beskrivningsf�ltet kan inte inneh�lla < eller > !"));
		m_bDontBother = FALSE;
		return;
	}


	pDoc->m_bNew = FALSE;
	m_bNew = FALSE;


	// open the info-dialog
	// the programsdlg should be updated aswell
	CDocTemplate *pTemplate;
	CWinApp* pApp = AfxGetApp();
	CString csDocName, csResStr, csDocTitle;
	csResStr.LoadString(IDR_PROGRAMS);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(csDocName, CDocTemplate::docName);
		ASSERT(pTemplate != NULL);
		csDocName = '\n' + csDocName;

		if (pTemplate && csDocName.Compare(csResStr) == 0)
		{
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			while(posDOC != NULL)
			{
				CMDISampleSuiteDoc* pDocument = (CMDISampleSuiteDoc*)pTemplate->GetNextDoc(posDOC);
				POSITION pos = pDocument->GetFirstViewPosition();
				if(pos != NULL && pDocument->m_nType == pDoc->m_nType)
				{
					CProgramsDlg* pView = (CProgramsDlg*)pDocument->GetNextView(pos);

					PROGRAM prg;
					LANGUAGE lng;
					int nStartIndex=0, nID, nProgIndex=0, nLangIndex=0;
					BOOL bFoundPrg = FALSE, bFoundLng = FALSE;;

					nID = _tstoi(m_csID);

					for(nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)
					{
						prg = theApp.m_caPrograms.GetAt(nLoop);

						if(prg.csName == m_csName && prg.nID == nID && prg.nProgtype == m_nType)
						{
							bFoundPrg = TRUE;
							nProgIndex = nLoop;
							break;
						}
						else if(prg.csName != m_csName && prg.nID == nID && prg.nProgtype == m_nType)
						{
							CString csBuf;
							csBuf.Format(g_pXML->str(1025), prg.nID, prg.csName);
							m_bDontBother = TRUE;
							AfxMessageBox(csBuf);
							m_bDontBother = FALSE;
							return;
						}

						nStartIndex += prg.nLangs;
					}

					((CComboBox*)GetDlgItem(IDC_COMBO_LANGUAGE))->GetLBText(m_nLanguage, m_csLang);
					m_csLang = m_cLocale.GetLangAbbr(m_csLang);

					// save the current data
//					if(m_bNew == TRUE)	// new
//					{
						if(bFoundPrg == TRUE)
						{
							// update existing program
							for(nLoop=0; nLoop<prg.nLangs; nLoop++)
							{
								lng = theApp.m_caLanguages.GetAt(nStartIndex + nLoop);

								if(lng.csLang == m_csLang)
								{
									bFoundLng = TRUE;
									break;
								}
							}
							nLangIndex = nStartIndex + nLoop;

							if(bFoundLng == TRUE)
							{
								// update the existing language
								lng.csLang = m_csLang;
								lng.csDesc = m_csDescription;
//								lng.nVersion = atof(m_cLocale.FixLocale(m_csVersion)) * 10.0;
								lng.csVersion = m_csVersion;
								lng.csPath.Format(_T("%s%s.hmi"), m_csID,  lng.csLang.MakeUpper());
								lng.csDate = m_ctDate.Format(_T("%Y-%m-%d")); //m_csDate;
								lng.bVisible = !m_bInvisible;
								theApp.m_caLanguages.SetAt(nLangIndex, lng);

								if(m_bNeedLicense == TRUE) prg.nLicense = 1;
								else prg.nLicense = 0;
								prg.csName = m_csName;
//								prg.nLangs = 1;
								prg.nID = _tstoi(m_csID);
								prg.nKeys = 0;
								prg.nProgtype = m_nType;
								prg.csArtNum = m_csArtNum;

								theApp.m_caPrograms.SetAt(nProgIndex, prg);
							}
							else
							{
								// add new language
								lng.csLang = m_csLang;
								lng.csDesc = m_csDescription;
//								lng.nVersion = atof(m_cLocale.FixLocale(m_csVersion)) * 10.0;
								lng.csVersion = m_csVersion;
								lng.csPath.Format(_T("%s%s.hmi"), m_csID,  lng.csLang.MakeUpper());
								lng.csDate = m_ctDate.Format(_T("%Y-%m-%d"));	//m_csDate;
								lng.bVisible = !m_bInvisible;
								theApp.m_caLanguages.InsertAt(nLangIndex, lng);

								prg.nLangs++;
								if(m_bNeedLicense == TRUE) prg.nLicense = 1;
								else prg.nLicense = 0;
								prg.csName = m_csName;
//								prg.nLangs = 1;
								prg.nID = _tstoi(m_csID);
								prg.nKeys = 0;
								prg.nProgtype = m_nType;
								prg.csArtNum = m_csArtNum;
	
								theApp.m_caPrograms.SetAt(nProgIndex, prg);
							}
						}
						else
						{
							// add new language
							lng.csLang = m_csLang;
							lng.csDesc = m_csDescription;
//							lng.nVersion = atof(m_cLocale.FixLocale(m_csVersion)) * 10.0;
							lng.csVersion = m_csVersion;
							lng.csPath.Format(_T("%s%s.hmi"), m_csID,  lng.csLang.MakeUpper());
							lng.csDate = m_ctDate.Format(_T("%Y-%m-%d"));	//m_csDate;
							lng.bVisible = !m_bInvisible;
							int nLngIndex = theApp.m_caLanguages.Add(lng);

							// add new program
							prg.csName = m_csName;
							prg.nLangs = 1;
							prg.nID = _tstoi(m_csID);
							prg.nKeys = 0;
							prg.nProgtype = m_nType;
							prg.csArtNum = m_csArtNum;
							if(m_bNeedLicense == TRUE) prg.nLicense = 1;
							else prg.nLicense = 0;

							int nPrgIndex = theApp.m_caPrograms.Add(prg);

							theApp.m_nType = m_nType;
							if(m_nType == 0)
							{
								theApp.m_nSelLangDP = nLngIndex;
								theApp.m_nSelProgramDP = nPrgIndex;
							}
							else
							{
								theApp.m_nSelLangMC = nLngIndex;
								theApp.m_nSelProgramMC = nPrgIndex;
							}
						}
//					}
//					else	// old
//					{
//					}


					if(pView->SaveXML() == 0)
						pView->AddSampleRecords();

					// make the package and save the info
					CreatePackageXML();

					//posDOC = (POSITION)1;
					break;
				}
			}
		}
	}

}

void CProgramDlg::OnDropFiles(HDROP hDropInfo) 
{
	// TODO: Add your message handler code here and/or call default
	UINT nFiles = DragQueryFile(hDropInfo, (UINT)-1, NULL, 0);

	TCHAR szFileName[_MAX_PATH + 1];
	UINT nNames;
	for(nNames = 0; nNames < nFiles; nNames++)
	{
		::ZeroMemory(szFileName, _MAX_PATH + 1);
		::DragQueryFile(hDropInfo, nNames, (LPTSTR)szFileName, _MAX_PATH + 1);

		AfxMessageBox(szFileName);
//		AfxGetApp()->OpenDocumentFile(szFileName);
	}

	CView::OnDropFiles(hDropInfo);
}

int CProgramDlg::OpenPackage()
{
	int nIndex = -1, nType=0;
	nIndex = g_pZIP->FindFile(_T("package.xml"), CZipArchive::ffNoCaseSens, true);

	// we got a package.xml?
	if(nIndex != -1)
	{
		g_pZIP->ExtractFile(nIndex, theApp.m_csTemppath);

		// parse the package xml-file
		XMLNode xNode = XMLNode::openFileHelper(theApp.m_csTemppath + _T("package.xml"), _T("XML"));

		if(xNode.isEmpty() != 1)
		{
			xNode = xNode.getChildNode(_T("package"));	// get the first tag, "package"
			if(xNode.isEmpty() != 1)
			{
				XMLNode xChild;
				CString csBuf, csFile;
				int i, iterator=0;
				BOOL bDoc = FALSE;

				csBuf = xNode.getAttribute(_T("date"));
				if(csBuf != _T("")) m_csDate = csBuf;

				int n = xNode.nChildNode(_T("file"));
				for(i=0; i<n; i++)
				{
					xChild = xNode.getChildNode(_T("file"), &iterator);

					// extract the files found in the xml file.
					nIndex = -1;
					csFile = xChild.getAttribute(_T("name"));
					nIndex = g_pZIP->FindFile(csFile, CZipArchive::ffNoCaseSens, true);

					if(nIndex != -1)
					{
						csBuf = xChild.getAttribute(_T("type"));
						if(csBuf == _T("DP")) nType = 1;
						else if(csBuf == _T("DPDOC")) nType = 2;
						else if(csBuf == _T("GENERIC")) nType = 3;

						m_wndReport.AddRecord(new CKeyRecord(csFile, nType));
						m_wndReport.Populate();
					}

				}
			}
		}

		// delete the xml-file
		DeleteFile(theApp.m_csTemppath + _T("package.xml"));
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

void CProgramDlg::OnReportFilebutton(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify != NULL);

	UpdateData(TRUE);

	if(m_csID == _T("") || m_csName == _T("") || m_csVersion == _T("") || m_nLanguage == -1)
	{
		m_bDontBother = TRUE;
		AfxMessageBox(_T("Du har inte fyllt i tillr�ckligt med information!"));
		m_bDontBother = FALSE;
		return;
	}

	if(pItemNotify->pItem->GetItemData() == 0)
	{
		m_bDontBother = TRUE;
		CFileDialog dlg(TRUE, NULL, _T(""), OFN_EXPLORER);
		if(dlg.DoModal() == IDOK)
		{
			int nProg, nLang;
			CString csName;
			CString csDestpath;
			CString csPath;
			csDestpath = theApp.m_csLocalPath;

			if(pDoc->m_nType == 0)	// digitech pro
			{
				nProg = theApp.m_nSelProgramDP;
				nLang = theApp.m_nSelLangDP;
				csName = theApp.m_csSelProgramDP;
			}
			else if(pDoc->m_nType == 1)	// mantax computer
			{
				nProg = theApp.m_nSelProgramMC;
				nLang = theApp.m_nSelLangMC;
				csName = theApp.m_csSelProgramMC;
			}

			LANGUAGE lng;

			if(pDoc->m_bNew == FALSE)
			{
				lng = theApp.m_caLanguages.GetAt(nLang);
				csPath = lng.csPath;
			}
			else
			{
				((CComboBox*)GetDlgItem(IDC_COMBO_LANGUAGE))->GetLBText(m_nLanguage, m_csLang);
				m_csLang = m_cLocale.GetLangAbbr(m_csLang);
				csPath.Format(_T("%s%s.hmi"), m_csID,  m_csLang.MakeUpper());
			}

			// open the package
			CFile fh;
		
			TRY
			{
				if(fh.Open(csDestpath + _T("\\") + csPath, CFile::modeRead) != 0)
				{
					fh.Close();
					g_pZIP->Open(csDestpath /*+ _T("\\")*/ + csPath);
				}
				else
				{
					g_pZIP->Open(csDestpath /*+ _T("\\")*/ + csPath, CZipArchive::zipCreate);
				}

				// delete any old file in this index from the package
				if(pItemNotify->pItem->GetCaption(pItemNotify->pColumn) != _T(""))
				{
					int nIndex = -1;
					nIndex = g_pZIP->FindFile(pItemNotify->pItem->GetCaption(pItemNotify->pColumn), CZipArchive::ffNoCaseSens, true);

					if(nIndex != -1)
					{
						g_pZIP->RemoveFile(nIndex);
					}
				}
				else	// add a new line to the report
				{
					m_wndReport.AddRecord(new CKeyRecord(_T(""), 0));
					m_wndReport.Populate();
				}

				// add the new file to the package
				pItemNotify->pItem->SetCaption(dlg.GetFileName());
				g_pZIP->AddNewFile(dlg.GetPathName(), dlg.GetFileName());

				// close the package
				g_pZIP->Close();
			}

			CATCH(CException, pEx)
			{
				TCHAR   szCause[255];
				CString strFormatted;

				pEx->GetErrorMessage(szCause, 255);
				strFormatted = _T("Error: ");
				strFormatted += szCause;

				AfxMessageBox(strFormatted);
			}
			END_CATCH


			// save the whole lot
			m_bDontBother = FALSE;
//			OnSave();
		}
	}
}

int CProgramDlg::CreatePackageXML()
{
	CFile fh;
	TCHAR szBuf[1024];

	if(fh.Open(theApp.m_csTemppath + _T("package.xml"), CFile::modeCreate|CFile::modeWrite) != 0)
	{
		((CComboBox*)GetDlgItem(IDC_COMBO_LANGUAGE))->GetLBText(m_nLanguage, m_csLang);
		CString csLang = m_cLocale.GetLangAbbr(m_csLang);

//		_stprintf(szBuf, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\r\n");
		_stprintf(szBuf, _T("<?xml version=\"1.0\" encoding=\"UTF-16\"?>\r\n"));
		fh.Write(szBuf, _tcslen(szBuf) * sizeof(TCHAR));

		CString csBuf;
		if(m_nType == 0)	// DP
			csBuf = _T("DP");
		else if(m_nType == 1)
			csBuf = _T("MC");
		else if(m_nType == 2)
			csBuf = _T("D");
		else
			csBuf =_T("OTHER");

//		_stprintf(szBuf, _T("\t<package id=\"%s\" name=\"%s\" lang=\"%s\" version=\"%s\" description=\"%s\" date=\"%s\" type=\"%s\">\r\n"),
		// tagit bort tab	och lagt till attribut f�r artikelnummer
		_stprintf(szBuf, _T("<package id=\"%s\" name=\"%s\" lang=\"%s\" version=\"%s\" description=\"%s\" date=\"%s\" type=\"%s\" artnum=\"%s\">\r\n"),
			m_csID, m_csName, csLang, m_csVersion, m_csDescription, m_ctDate.Format(_T("%Y-%m-%d")) /*m_csDate*/, csBuf, m_csArtNum);
		fh.Write(szBuf, _tcslen(szBuf) * sizeof(TCHAR));

		// get the files from the report
		CXTPReportRecords* pRecords = m_wndReport.GetRecords();
		CXTPReportRecord* pRecord;
		CString csName, csType;

		for(int nLoop=0; nLoop<pRecords->GetCount(); nLoop++)
		{
			pRecord = pRecords->GetAt(nLoop);

			csName = pRecord->GetItem(0)->GetCaption(0);
			csType = pRecord->GetItem(1)->GetCaption(0);

			if(csName != _T("") && csType != _T(""))
			{
//				_stprintf(szBuf, _T("\t\t<file name=\"%s\" type=\"%s\"/>\r\n"), csName, csType);
				// tagit bort tab	
				_stprintf(szBuf, _T("<file name=\"%s\" type=\"%s\"/>\r\n"), csName, csType);
				fh.Write(szBuf, _tcslen(szBuf) * sizeof(TCHAR));
			}
		}

//		_stprintf(szBuf, _T("\t</package>\r\n"));
		// tagit bort tab	
		_stprintf(szBuf, _T("</package>\r\n"));
		fh.Write(szBuf, _tcslen(szBuf) * sizeof(TCHAR));

		fh.Close();


		// update the package aswell
		CString csDestpath = theApp.m_csLocalPath;
		CString csPath;
		((CComboBox*)GetDlgItem(IDC_COMBO_LANGUAGE))->GetLBText(m_nLanguage, m_csLang);
		m_csLang = m_cLocale.GetLangAbbr(m_csLang);
		csPath.Format(_T("%s%s.hmi"), m_csID,  m_csLang);

		// open the package
		if(fh.Open(csDestpath + csPath, CFile::modeRead) == 0)
		{
			// create a new archive
			g_pZIP->Open(csDestpath + csPath, CZipArchive::zipCreate);
		}
		else
		{
			// open existing archive
			fh.Close();
			g_pZIP->Open(csDestpath + csPath);
		}


		int nIndex = -1;
		nIndex = g_pZIP->FindFile(_T("package.xml"), CZipArchive::ffNoCaseSens, true);

		if(nIndex != -1)
		{
			g_pZIP->RemoveFile(nIndex);
		}

		g_pZIP->AddNewFile(theApp.m_csTemppath + _T("package.xml"), _T("package.xml"));

		DeleteFile(theApp.m_csTemppath + _T("package.xml"));

		g_pZIP->Close();
	}
	else
		return FALSE;

	return TRUE;
}

// turn on/off the trashcan-icon
void CProgramDlg::OnReportSelChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	CXTPReportRecord* pRecord = m_wndReport.GetFocusedRow()->GetRecord();
	if (pRecord)
	{
		CString csBuf = pRecord->GetItem(0)->GetCaption(0);

		if(csBuf != _T(""))
		{
			// lit the trashcan
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, TRUE);
		}
		else
		{
			// disable trashcan
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
		}
	}
}

// delete selected program from the package
void CProgramDlg::OnDelete()
{
	CXTPReportRecord* pRecord = m_wndReport.GetFocusedRow()->GetRecord();
	if (pRecord)
	{
		CString csBuf = pRecord->GetItem(0)->GetCaption(0);	// filename

		// open archive
		((CComboBox*)GetDlgItem(IDC_COMBO_LANGUAGE))->GetLBText(m_nLanguage, m_csLang);
		CString csLang = m_cLocale.GetLangAbbr(m_csLang);
		CString csPath;
		csPath.Format(_T("%s%s.hmi"), m_csID,  csLang);

		g_pZIP->Open(theApp.m_csLocalPath + csPath);

		int nIndex = g_pZIP->FindFile(csBuf, CZipArchive::ffNoCaseSens, true);

		if(nIndex != -1)
		{
			g_pZIP->RemoveFile(nIndex);

			// update the report aswell
			nIndex = pRecord->GetIndex();
			m_wndReport.GetRecords()->RemoveAt(nIndex);
			m_wndReport.Populate();
		}

		g_pZIP->Close();
	}
}
