// ProgramsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "LicenseReportDialog.h"
#include "ProgramsDlg.h"
#include "SampleSuiteForms.h"
#include "Usefull.h"
#include "SerialDlg.h"
#include "ExtraLicenseReportInfoDlg.h"
#include "XBrowseForFolder.h"
#include "DP_Code.h"


IMPLEMENT_DYNCREATE(CProgramsFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CProgramsFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CProgramsFrame)
	ON_WM_CREATE()
	ON_WM_MDIACTIVATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMsgSuite)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
};

/////////////////////////////////////////////////////////////////////////////
// CProgramsFrame construction/destruction

CProgramsFrame::CProgramsFrame()
{
	m_bOnce = TRUE;
	m_bLock = FALSE;
}

CProgramsFrame::~CProgramsFrame()
{
}

LRESULT CProgramsFrame::OnMsgSuite(WPARAM wParm, LPARAM lParm)
{
	// user pushed some buttons in the shell.
	switch(wParm)
	{
		case ID_NEW_ITEM:
			((CProgramsDlg*)GetActiveView())->OnNew();
		break;

		case ID_OPEN_ITEM:
		break;

		case ID_PREVIEW_ITEM:
		break;

		case ID_SAVE_ITEM:
		break;

		case ID_DELETE_ITEM:
			((CProgramsDlg*)GetActiveView())->OnDelete();
		break;
	};

	return 0;
}

void CProgramsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce)
    {
		m_bOnce = false;

		CString csBuf;
		csBuf.Format(_T("%s\\HMS_Development\\Dialogs\\Programs"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

void CProgramsFrame::OnClose()
{
	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);

	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

void CProgramsFrame::OnDestroy()
{
	CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\HMS_Development\\Dialogs\\Programs"), REG_ROOT);
	SavePlacement(this, csBuf);
	m_bOnce = TRUE;

	RemoveLockfile();
}

BOOL CProgramsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CProgramsFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
        RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

/////////////////////////////////////////////////////////////////////////////
// CProgramsFrame diagnostics

#ifdef _DEBUG
void CProgramsFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CProgramsFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CProgramsFrame message handlers

int CProgramsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	// check if we have a valid license.
/*	if( theApp.CheckLicense(_T("H2020C")) == TRUE )
	{
		theApp.m_bWriteAllowed = TRUE;
		CreateLockfile();
	}
	else
	{
		theApp.m_bWriteAllowed = FALSE;
	}*/
	theApp.m_bWriteAllowed = FALSE;

	if(CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this, 0);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR_PROGRAMS);

	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getProgDir() + _T("HMSIcons.icl");

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{
				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR_PROGRAMS)
				{		
						// view licenses/info
						pCtrl = p->GetAt(0);
						pCtrl->SetTooltip(g_pXML->str(1000));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 20);	// info
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);

						// send program to caliper
						pCtrl = p->GetAt(1);
						pCtrl->SetTooltip(g_pXML->str(1001));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 5);	// comto
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);

						// show the manual
						pCtrl = p->GetAt(2);
						pCtrl->SetTooltip(g_pXML->str(1002));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 13);	// gem
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);

						// refresh
						pCtrl = p->GetAt(3);
						pCtrl->SetTooltip(g_pXML->str(1008));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 43);	// refresh
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						//pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(TRUE);

						//select files to send
						pCtrl = p->GetAt(4);
						pCtrl->SetTooltip(g_pXML->str(1009));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 9);	// export
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						//pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(TRUE);

						//#3756 send to dp2
						pCtrl = p->GetAt(5);
						pCtrl->SetTooltip(_T("DPII"));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 66);	// 
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						//pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(TRUE);

						//send to dp3
						pCtrl = p->GetAt(6);
						pCtrl->SetTooltip(_T("DPII+"));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 0);	// +
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						//pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(TRUE);

				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))


	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	UpdateWindow();

	return 0;
}

void CProgramsFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);

	CSize sz(0);
	if (m_wndToolBar.GetSafeHwnd())
	{
		RECT rect;
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, LM_HORZDOCK|LM_HORZ|LM_COMMIT);

		m_wndToolBar.MoveWindow(0, 0, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
	}
}

void CProgramsFrame::SetButtonEnabled(UINT nID, BOOL bEnabled)
{
	CXTPToolBar* pToolBar = &m_wndToolBar;
	CXTPControls* p = pToolBar->GetControls();
	CXTPControl* pCtrl = NULL;

	pCtrl = p->GetAt(nID);
	pCtrl->SetEnabled(bEnabled);
}

int CProgramsFrame::CreateLockfile()
{
	CFile fh;
	CString csUser;
	char szBuf[256];

	if(fh.Open(theApp.m_csLocalPath + _T("programs.lck"), CFile::modeRead, NULL))
	{
		memset(szBuf, 0, 255);
		fh.Read(szBuf, 255);
		csUser = szBuf;

		fh.Close();
		m_bLock = TRUE;
		
		CString csBuf;
		csBuf.Format(_T("User \"%s\" is already editing the programs.xml.\nIf this is not the case, remove the programs.lck file."), csUser, csUser);
		AfxMessageBox(csBuf);

		return FALSE;
	}

	m_bLock = FALSE;
	fh.Open(theApp.m_csLocalPath + _T("programs.lck"), CFile::modeCreate|CFile::modeWrite, NULL);
	csUser = regGetStr(_T("Software\\Microsoft\\Windows\\CurrentVersion"), _T("Explorer"), _T("Logon User Name"), _T(""));
	sprintf(szBuf, "%S", csUser);
	fh.Write(szBuf, strlen(szBuf));	// get current user- and computername.
	fh.Close();

	return TRUE;
}

int CProgramsFrame::RemoveLockfile()
{
	if(m_bLock == TRUE || theApp.m_bWriteAllowed == FALSE) return FALSE;

	if(!DeleteFile(theApp.m_csLocalPath + _T("programs.lck")))
	{
		AfxMessageBox(_T("Can not delete lock-file!"));
		return FALSE;
	}

	return TRUE;
}

/*---------------------------------------------------------------------*/
#include "MessageRecord.h"
#include ".\programsdlg.h"

#define COLUMN_CHECK 0
#define COLUMN_PROGRAM	1
#define COLUMN_VERSION	2
//#define COLUMN_NEWVERSION 2
#define COLUMN_LANGUAGE	3
#define COLUMN_TLA	4
#define COLUMN_PID	5


/////////////////////////////////////////////////////////////////////////////
// CProgramsDlg

IMPLEMENT_DYNCREATE(CProgramsDlg, CXTPReportView)

BEGIN_MESSAGE_MAP(CProgramsDlg, CXTPReportView)
	//{{AFX_MSG_MAP(CProgramsDlg)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_BN_CLICKED(ID_BUTTON_PROGRAM_OPEN, OnBnClickedLicense)
	ON_BN_CLICKED(ID_BUTTON_PROGRAM_DOWNLOAD, OnBnClickedDownload)
	ON_BN_CLICKED(ID_BUTTON_PROGRAM_MANUAL, OnBnClickedManual)
	ON_BN_CLICKED(ID_BUTTON_PROGRAM_REFRESH, OnBnClickedRefresh)
	ON_BN_CLICKED(ID_BUTTON_PROGRAM_SELECT_FILES, OnBnClickedSelectFiles)
	ON_BN_CLICKED(ID_BUTTON_PROGRAM_SEND_DP2,OnBnClickedSendToDP2)
	ON_BN_CLICKED(ID_BUTTON_PROGRAM_SEND_DP3,OnBnClickedSendToDP3)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
	ON_NOTIFY(XTP_NM_REPORT_SELCHANGED, XTP_ID_REPORT_CONTROL, OnReportSelChanged)
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportSelChanged)
	ON_NOTIFY(XTP_NM_REPORT_HYPERLINK , XTP_ID_REPORT_CONTROL, OnReportHyperlinkClick)
	ON_NOTIFY(NM_RCLICK, XTP_ID_REPORT_CONTROL, OnReportItemRClick)
	ON_NOTIFY(NM_DBLCLK, XTP_ID_REPORT_CONTROL, OnReportItemDblClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
	ON_NOTIFY(NM_KEYDOWN, XTP_ID_REPORT_CONTROL, OnReportKeyDown)
	ON_NOTIFY(LVN_BEGINDRAG, XTP_ID_REPORT_CONTROL, OnReportBeginDrag)
	ON_NOTIFY(XTP_NM_REPORT_BEFORE_PASTE_FROMTEXT, XTP_ID_REPORT_CONTROL, OnReportBeforePasteFromText)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProgramsDlg construction/destruction

CProgramsDlg::CProgramsDlg()
{
	m_nVersionXML = 0;
	m_nSelProgram = -1;
	m_nSelLang = -1;
}

CProgramsDlg::~CProgramsDlg()
{
}

BOOL CProgramsDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CView::PreCreateWindow(cs))
		return FALSE;

	//cs.dwExStyle |= WS_EX_STATICEDGE;
	//cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

/////////////////////////////////////////////////////////////////////////////
// CProgramsDlg diagnostics

#ifdef _DEBUG
void CProgramsDlg::AssertValid() const
{
	CView::AssertValid();
}

void CProgramsDlg::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
/*
CDocument* CProgramsDlg::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDocument)));
	return (CDocument*)m_pDocument;
}*/
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CProgramsDlg message handlers

int CProgramsDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTPReportView::OnCreate(lpCreateStruct) == -1)
		return -1;

//	LoadReportState();

	// resize the window to match the frame
//	RECT rect;
//	GetParentFrame()->GetClientRect(&rect);
//	OnSize(1, rect.right, rect.bottom);

	return 0;
}

void CProgramsDlg::OnDestroy()
{
//	SaveReportState();

/*	if (m_pTaskFrame)
	{
		m_pTaskFrame->DestroyWindow();
	}
	if (m_pPropertiesFrame)
	{
		m_pPropertiesFrame->DestroyWindow();
	}
*/

	CView::OnDestroy();
}


void CProgramsDlg::OnInitialUpdate()
{
	// check if we have a valid license.
	if( theApp.CheckLicense(_T("H2020A")) == FALSE)
	{
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4,
			(LPARAM)&_user_msg(820, _T("OpenSuiteEx"),	
			_T("License.dll"),
			_T(""),
			_T(""),
			_T("")));

		GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);

		return;
	}

	CView::OnInitialUpdate();

	// get a handle to the document
	pDoc = (CMDISampleSuiteDoc*)GetDocument();

	CXTPReportControl& wndReport = GetReportCtrl();
	wndReport.SetImageList(&m_ilIcons);

	//
	//  Add columns
	//
	CXTPReportColumn* pCol;
	pCol = wndReport.AddColumn(new CXTPReportColumn(COLUMN_CHECK, _T(""), 5));

	pCol->AllowRemove(FALSE);
	pCol = wndReport.AddColumn(new CXTPReportColumn(COLUMN_PROGRAM, g_pXML->str(1003), 100));
	pCol->AllowRemove(FALSE);
	pCol = wndReport.AddColumn(new CXTPReportColumn(COLUMN_VERSION, g_pXML->str(1004), 101));
	pCol->AllowRemove(FALSE);
	pCol = wndReport.AddColumn(new CXTPReportColumn(COLUMN_LANGUAGE, g_pXML->str(1006), 103));
	pCol->AllowRemove(FALSE);
	pCol = wndReport.AddColumn(new CXTPReportColumn(COLUMN_TLA, g_pXML->str(1006), 104));
	pCol->SetVisible(FALSE);
	pCol->AllowRemove(FALSE);
	pCol = wndReport.AddColumn(new CXTPReportColumn(COLUMN_PID, g_pXML->str(1007), 105));
	pCol->AllowRemove(FALSE);


	//
	//  Add records
	//
	ParseXML();
	AddSampleRecords();

	// Set the report to group by Language
	pCol = GetReportCtrl().GetColumns()->Find(COLUMN_LANGUAGE);
	GetReportCtrl().GetColumns()->GetGroupsOrder()->Clear();
	GetReportCtrl().GetColumns()->GetGroupsOrder()->Add(pCol);
	pCol->SetVisible(FALSE);

	// populate the report
	GetReportCtrl().Populate();
	wndReport.CollapseAll();
	GetReportCtrl().ShowGroupBy(!GetReportCtrl().IsGroupByVisible());


	CString csBuf;
	csBuf.Format(_T("%s %d.%02d"), g_pXML->str(1024), m_nVersionXML/100, m_nVersionXML%100);
	((CProgramsFrame*)GetParentFrame())->m_wndStatusBar.SetWindowText(csBuf);

/*
	CProgramsFrame* pWnd = (CProgramsFrame*)AfxGetMainWnd();
	if (m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNLIST, &pWnd->m_wndFieldChooser);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}

	if (m_wndFilterEdit.GetSafeHwnd() == NULL)
	{
		m_wndFilterEdit.SubclassDlgItem(IDC_FILTEREDIT, &pWnd->m_wndFilterEdit);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetFilterEditCtrl(&m_wndFilterEdit);
	}
*/
}

void CProgramsDlg::LoadReportState()
{
#ifdef XML_STATE
	CXTPPropExchangeXMLNode px(TRUE, 0, _T("ReportControl"));
	if (!px.LoadFromFile(_T("c:\\ReportControl.xml")))
		return;
	
	GetReportCtrl().DoPropExchange(&px);

#else	
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary(_T("ReportControl"), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
#endif
}

void CProgramsDlg::SaveReportState()
{
#ifdef XML_STATE
	
	CXTPPropExchangeXMLNode px(FALSE, 0, _T("ReportControl"));
	GetReportCtrl().DoPropExchange(&px);

	// Save All Records
	//CXTPPropExchangeSection secRecords(px.GetSection(_T("Records")));
	//GetReportCtrl().GetRecords()->DoPropExchange(&secRecords);

	px.SaveToFile(_T("c:\\ReportControl.xml"));

#else
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary(_T("ReportControl"), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
#endif

}


#define ID_REMOVE_ITEM  1
#define ID_SORT_ASC     2
#define ID_SORT_DESC        3
#define ID_GROUP_BYTHIS 4
#define ID_SHOW_GROUPBOX        5
#define ID_SHOW_FIELDCHOOSER 6
#define ID_COLUMN_BESTFIT       7
#define ID_COLUMN_ARRANGEBY 100
#define ID_COLUMN_ALIGMENT  200
#define ID_COLUMN_ALIGMENT_LEFT ID_COLUMN_ALIGMENT + 1
#define ID_COLUMN_ALIGMENT_RIGHT    ID_COLUMN_ALIGMENT + 2
#define ID_COLUMN_ALIGMENT_CENTER   ID_COLUMN_ALIGMENT + 3
#define ID_COLUMN_SHOW      500


CString LoadResourceString(UINT nID)
{
	CString str;
	VERIFY(str.LoadString(nID));
	return str;
}

void CProgramsDlg::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;
}

void CProgramsDlg::OnReportBeginDrag(NMHDR * /*pNotifyStruct*/, LRESULT * /*result*/)
{
	int nCount = GetReportCtrl().GetSelectedRows()->GetCount();
	for (int i = nCount - 1; i >= 0; i--)
	{
		CXTPReportRow* pRow = GetReportCtrl().GetSelectedRows()->GetAt(i);
		if (pRow->IsGroupRow())
		{
			pRow->SetExpanded(TRUE);
			pRow->SelectChilds();
		}
	}

	COleDataSource ds;
	ds.DoDragDrop(DROPEFFECT_COPY|DROPEFFECT_MOVE);
}


void CProgramsDlg::OnReportItemRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
}

void CProgramsDlg::OnReportHyperlinkClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if (!pItemNotify->pRow || !pItemNotify->pColumn)
		return;
	// if click on Hyperlink in Item
	if (pItemNotify->nHyperlink >= 0)
		TRACE(_T("Hyperlink Click : \n row %d \n col %d \n Hyperlink %d\n"),
				pItemNotify->pRow->GetIndex(), pItemNotify->pColumn->GetItemIndex(), pItemNotify->nHyperlink);
}

void CProgramsDlg::OnReportSelChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	CXTPReportControl& wndReport = GetReportCtrl();
	CXTPReportRow* pRow = wndReport.GetFocusedRow();

	if (!pRow) return;

	int nItem = pRow->GetIndex();

	PROGRAM prg;
	LANGUAGE lng;
	CString csName, csLang;
	CString csVersion;
	int nLoop, nLoop2, nLangIndex=0;
	BOOL bFoundPrg=FALSE, bFoundLang=FALSE;

	CMessageRecord* pRecord = DYNAMIC_DOWNCAST(CMessageRecord, pRow->GetRecord());
	if (pRecord)
	{
		CXTPReportRecordItem* pItem = pRecord->GetItem(COLUMN_PROGRAM);	// name
		csName = pItem->GetCaption(0);

		pItem = pRecord->GetItem(COLUMN_VERSION);	// version
//		nVersion = (atof(m_cLocale.FixLocale(m_cLocale.FixLocale(pItem->GetCaption(0)))) * 10.0);
		csVersion = pItem->GetCaption(0);
		csVersion = csVersion.Left(csVersion.Find(_T(" ("), 0));

		pItem = pRecord->GetItem(COLUMN_TLA);	// language
		csLang = pItem->GetCaption(0);


		// search for the program
		for(nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)
		{
			prg = theApp.m_caPrograms.GetAt(nLoop);

			if(prg.nID == pRecord->m_nProgID &&
				prg.csName == csName)
			{
				// search for the language
				for(nLoop2=0; nLoop2<prg.nLangs; nLoop2++)
				{
					lng = theApp.m_caLanguages.GetAt(nLangIndex + nLoop2);

					if(lng.csLang == csLang)
					{
						bFoundLang = TRUE;
						break;
					}
				}

				bFoundPrg = TRUE;
				break;
			}
	
			nLangIndex += prg.nLangs;
		}

		if(bFoundPrg == FALSE || bFoundLang == FALSE) return;

		m_nSelProgram = nLoop;
		m_nSelLang = nLangIndex + nLoop2;

		// set icons according to the choice made.
		if(((CProgramsFrame*)GetParent())->m_bLock == FALSE && theApp.m_bWriteAllowed == TRUE)
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, TRUE);

		// enable the download- and info-buttons.
		if(theApp.m_bWriteAllowed == TRUE)
			((CProgramsFrame*)GetParent())->SetButtonEnabled(0, TRUE);	// info
		else
			((CProgramsFrame*)GetParent())->SetButtonEnabled(0, FALSE);	// info
		((CProgramsFrame*)GetParent())->SetButtonEnabled(1, TRUE);	// send
		((CProgramsFrame*)GetParent())->SetButtonEnabled(2, TRUE);	// manual
	}
}

void CProgramsDlg::OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;

	if (!GetReportCtrl().GetFocusedRow())
		return;

	if (lpNMKey->nVKey == VK_RETURN)
	{
		CMessageRecord* pRecord = DYNAMIC_DOWNCAST(CMessageRecord, GetReportCtrl().GetFocusedRow()->GetRecord());
		if (pRecord)
		{
			if (pRecord->SetRead())
			{
				GetReportCtrl().Populate();
			}
		}
	}
}

void CProgramsDlg::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	if(theApp.m_bWriteAllowed == FALSE) return;

	CXTPReportRow* pRow = GetReportCtrl().GetFocusedRow();

	if(pRow)
	{
		int nItem = 0, nProg=0, nLang=0;
		CString csBuf;

		// find out which program we have chosen.
		nItem = /*pItemNotify->*/pRow->GetIndex();

		PROGRAM prg;
		LANGUAGE lng;
		CString csName, csLang;
		CString csVersion;
		int nLoop, nLoop2, nLangIndex=0;
		BOOL bFoundPrg=FALSE, bFoundLang=FALSE;

		CMessageRecord* pRecord = DYNAMIC_DOWNCAST(CMessageRecord, /*pItemNotify->*/pRow->GetRecord());
		if (pRecord)
		{
			CXTPReportRecordItem* pItem = pRecord->GetItem(COLUMN_PROGRAM);	// name
			csName = pItem->GetCaption(0);

			pItem = pRecord->GetItem(COLUMN_VERSION);	// version
//			nVersion = (atof(m_cLocale.FixLocale(m_cLocale.FixLocale(pItem->GetCaption(0)))) * 10.0);
			csVersion = pItem->GetCaption(0);
			csVersion = csVersion.Left(csVersion.Find(_T(" ("), 0));

			pItem = pRecord->GetItem(COLUMN_TLA);	// language
			csLang = pItem->GetCaption(0);

			// search for the program
			for(nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)
			{
				prg = theApp.m_caPrograms.GetAt(nLoop);

				if(prg.nID == pRecord->m_nProgID &&
					prg.csName == csName)
				{
					// search for the language
					for(nLoop2=0; nLoop2<prg.nLangs; nLoop2++)
					{
						lng = theApp.m_caLanguages.GetAt(nLangIndex + nLoop2);

						if(lng.csVersion == csVersion &&	//lng.nVersion == nVersion &&
							lng.csLang == csLang)
						{
							nLang = nLangIndex + nLoop2;
							bFoundLang = TRUE;
							break;
						}
					}

					nProg = nLoop;
					bFoundPrg = TRUE;
					break;
				}

				nLangIndex += prg.nLangs;
			}

			if(bFoundPrg == FALSE || bFoundLang == FALSE) return;

			// send the indexes to the info-dialog.
			if(pDoc->m_nType == 0)	// digitech pro
			{
				theApp.m_nSelProgramDP = nProg;
				theApp.m_nSelLangDP = nLang;
				theApp.m_csSelProgramDP = csName;
			}
			else if(pDoc->m_nType == 1)	// mantax computer
			{
				theApp.m_nSelProgramMC = nProg;
				theApp.m_nSelLangMC = nLang;
				theApp.m_csSelProgramMC = csName;
			}

			// open the info-dialog
			CDocTemplate *pTemplate;
			CWinApp* pApp = AfxGetApp();
			CString csDocName, csResStr;
			csResStr.LoadString(IDR_PROGRAM);

			POSITION pos = pApp->GetFirstDocTemplatePosition();
			while(pos != NULL)
			{
				pTemplate = pApp->GetNextDocTemplate(pos);
				pTemplate->GetDocString(csDocName, CDocTemplate::docName);
				ASSERT(pTemplate != NULL);
				// Need to add a linefeed, infront of the docName.
				// This is because, for some reason, the document title,
				// set in resource, must have a linefeed.
				// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
				csDocName = '\n' + csDocName;

				if (pTemplate && csDocName.Compare(csResStr) == 0)
				{
					POSITION posDOC = pTemplate->GetFirstDocPosition();
					while(posDOC != NULL)
					{
						CMDISampleSuiteDoc* pDocument = (CMDISampleSuiteDoc*)pTemplate->GetNextDoc(posDOC);
						POSITION pos = pDocument->GetFirstViewPosition();
						if(pos != NULL && pDocument->m_nType == pDoc->m_nType)
						{
							pDocument->m_bNew = FALSE;
							pDocument->m_bClick = FALSE;
							pDocument->m_bLock = ((CProgramsFrame*)GetParent())->m_bLock;
							CView* pView = pDocument->GetNextView(pos);
							pView->GetParent()->BringWindowToTop();
							pView->GetParent()->SetFocus();
							posDOC = (POSITION)1;
							break;
						}
					}

					if(posDOC == NULL)
					{
						theApp.m_nType = pDoc->m_nType;
						theApp.m_bNew = FALSE;
						theApp.m_bClick = FALSE;
						theApp.m_bLock = ((CProgramsFrame*)GetParent())->m_bLock;
						CMDISampleSuiteDoc* pDocument = (CMDISampleSuiteDoc*)pTemplate->OpenDocumentFile(NULL);
						CString sDocTitle;
						pDocument->SetTitle(g_pXML->str(1000));
						pDocument->m_nType = pDoc->m_nType;
						pDocument->m_bLock = ((CProgramsFrame*)GetParent())->m_bLock;
					}
				}
			}

		}
	}
}

void CProgramsDlg::OnShowFieldChooser()
{
/*	CProgramsFrame* pMainFrm = (CProgramsFrame*)AfxGetMainWnd();
	if (pMainFrm)
	{
		BOOL bShow = !pMainFrm->m_wndFieldChooser.IsVisible();
		pMainFrm->ShowControlBar(&pMainFrm->m_wndFieldChooser, bShow, FALSE);
	}*/
}

void CProgramsDlg::AddSampleRecords()
{
	CXTPReportControl& wndReport = GetReportCtrl();
	CXTPReportRecords* pRecords = wndReport.GetRecords();
	pRecords->RemoveAll();

	// loop through all programs
	PROGRAM prg;
	LANGUAGE lng;
	int nStartLang=0, nLang;
	CString csVer, csLang, csBuf;

	for(int nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)
	{
		prg = theApp.m_caPrograms.GetAt(nLoop);

//csBuf.Format(_T("Progtype: %d, %d"), prg.nProgtype, pDoc->m_nType);
//AfxMessageBox(csBuf);

		if(prg.nProgtype == pDoc->m_nType)
		{
			// get the different versions/languages
			for(nLang=0; nLang<prg.nLangs; nLang++)
			{
				lng = theApp.m_caLanguages.GetAt(nStartLang + nLang);

				csBuf.Format(_T(" (%s)"), lng.csDate);
//				csVer.Format(_T("%d.%d"), lng.nVersion/10, lng.nVersion%10);
				csVer = lng.csVersion;
				if(lng.csDate != _T("")) csVer += csBuf;

				csLang = m_cLocale.GetLangString(lng.csLang);

				wndReport.AddRecord(new CMessageRecord(FALSE, prg.nID, prg.csName, csVer, csLang, lng.csLang));
			}
		}

		nStartLang += prg.nLangs;
	}

	GetReportCtrl().Populate();
}

void CProgramsDlg::OnReportBeforePasteFromText(NMHDR * pNotifyStruct, LRESULT * result)
{
	ASSERT(pNotifyStruct);
	
	XTP_NM_REPORT_BEFORE_COPYPASTE* pnmCopyPaste = (XTP_NM_REPORT_BEFORE_COPYPASTE*)pNotifyStruct; 
	
	if(!pnmCopyPaste || !pnmCopyPaste->ppRecord || !pnmCopyPaste->parStrings) {
		ASSERT(FALSE);
		return;		
	}

	CMessageRecord* pRecord = new CMessageRecord();
	if(!pRecord) {
		return;
	}
	
	*pnmCopyPaste->ppRecord = pRecord;
	
	CXTPReportColumns* pColumns = GetReportCtrl().GetColumns();
	ASSERT(pColumns);
	if(!pColumns) {
		return;
	}

	int nDataCount = pnmCopyPaste->parStrings->GetSize();

	const int nColumnCount = pColumns->GetVisibleColumnsCount();
	for (int nCol = 0; nCol < nColumnCount; nCol++)
	{
		CXTPReportColumn* pColumn = pColumns->GetVisibleAt(nCol);
		CXTPReportRecordItem* pItem = pRecord->GetItem(pColumn);
		ASSERT(pItem);
		if (NULL == pItem)
			continue;

		if(nCol < nDataCount) 
		{
			CString strItem = pnmCopyPaste->parStrings->GetAt(nCol);
			pItem->SetCaption(strItem);
		}
	}
}

/*---------------------------------------------------------------------*/
int CProgramsDlg::GetLocalXML()
{
	theApp.m_caPrograms.RemoveAll();
	theApp.m_caLanguages.RemoveAll();
	theApp.m_caKeys.RemoveAll();


	CString csDestpath;
	csDestpath = theApp.m_csLocalPath;

	XMLNode xNode = XMLNode::openFileHelper(csDestpath + _T("programs.xml"), _T("XML"));

	if(xNode.isEmpty() != 1)
	{
		xNode = xNode.getChildNode(_T("Programs"));	// get the first tag, "Programs"

		if(xNode.isEmpty() != 1)
		{

			PROGRAM prg;
			KEY key;
			LANGUAGE lng;
			CString csID, csName, csVersion, csPath, csDesc, csBuf, csArtNum;
			XMLNode xChild, xKey, xLang;
			int i, iterator=0, j, jterator=0, nKeys=0, nLangs=0, nProgtype, nNeedLic=0;

			// get the version and date of the XML
			csBuf = xNode.getChildNode(_T("Version")).getText();
			if(csBuf != _T("")) m_nVersionXML = _tstoi(csBuf);

			csBuf = xNode.getChildNode(_T("Date")).getText();
			if(csBuf != _T("")) m_csDateXML = csBuf;

			int n = xNode.nChildNode(_T("Product"));
			for(i=0; i<n; i++)
			{
				prg.nKeys = 0;
				prg.nLangs = 0;

				xChild = xNode.getChildNode(_T("Product"), &iterator);
				csID = xChild.getChildNode(_T("ID")).getText();
				csName = xChild.getChildNode(_T("Name")).getText();

				csBuf = xChild.getChildNode(_T("Progtype")).getText();
				if(csBuf != _T("")) nProgtype = _tstoi(csBuf);

				csBuf = xChild.getChildNode(_T("NeedLic")).getText();
				if(csBuf != _T("")) nNeedLic = _tstoi(csBuf);

				csArtNum = xChild.getChildNode(_T("ArtNum")).getText();

				nLangs = xChild.nChildNode(_T("Lang"));
				if(nLangs != 0)	// we have some languages
				{
					for(j=0; j<nLangs; j++)
					{
						xLang = xChild.getChildNode(_T("Lang"), &jterator);

						lng.csLang = xLang.getChildNode(_T("ID")).getText();
						lng.csVersion = xLang.getChildNode(_T("Version")).getText();
						lng.nVersionWeb = 0;
						lng.csPath = xLang.getChildNode(_T("Filename")).getText();
						lng.csPathWeb = _T("");
						lng.csDesc = xLang.getChildNode(_T("Description")).getText();
						lng.bLocal = TRUE;
						lng.csDate = xLang.getChildNode(_T("Date")).getText();
						csBuf = xLang.getChildNode(_T("Visible")).getText();
						if(csBuf != _T("")) lng.bVisible = _tstoi(csBuf);
						theApp.m_caLanguages.Add(lng);
						prg.nLangs++;
					}

					jterator = 0;
				}

				prg.csName = csName;
				prg.nID = _tstoi(csID);
				prg.nProgtype = nProgtype;
				prg.nLicense = nNeedLic;
				prg.csArtNum = csArtNum;
				theApp.m_caPrograms.Add(prg);
			}
		}
	}

	return 0;
}

int CProgramsDlg::GetWebXML()
{
	// retrieve the web XML.
	W3Client w3;
	CString csBuf, csBuf2;
	int nRet = 0, nRet2 = 0;

	CFile fh;
	if(fh.Open(theApp.m_csLocalPath + _T("programs.xml"), CFile::modeRead, 0))
	{
		fh.Read(csBuf.GetBufferSetLength(fh.GetLength()), fh.GetLength());
		fh.Close();
	}

	// parse the buffer
	XMLResults pResults;
	XMLNode xNode = XMLNode::parseString((LPCTSTR)csBuf.GetBuffer(), _T("XML"), &pResults);
	if(xNode.isEmpty() != 1)
	{
		PROGRAM prgWeb, prgLocal;
		LANGUAGE lng;
		BOOL bFound;

		xNode = xNode.getChildNode(_T("Programs"));	// get the first tag, "Programs"
		if(xNode.isEmpty() != 1)
		{
			// get the version and date of the XML
			csBuf = xNode.getChildNode(_T("Version")).getText();
			if(csBuf != _T("")) m_nVersionXML = _tstoi(csBuf);

			csBuf = xNode.getChildNode(_T("Date")).getText();
			if(csBuf != _T("")) m_csDateXML = csBuf;


			int n = xNode.nChildNode(_T("Product"));

			CString csID, csName, csVersion, csPath, csDesc, csArtNum;
			XMLNode xChild, xKey, xLang;
			int i, iterator=0, jterator=0, nProgtype, nLoop, nLangIndex=0, j, nNeedLic=0;

			for(i=0; i<n; i++)
			{
				xChild = xNode.getChildNode(_T("Product"), &iterator);

				csID = xChild.getChildNode(_T("ID")).getText();
				csName = xChild.getChildNode(_T("Name")).getText();

				csBuf = xChild.getChildNode(_T("Progtype")).getText();
				if(csBuf != _T("")) nProgtype = _tstoi(csBuf);

				csBuf = xChild.getChildNode(_T("NeedLic")).getText();
				if(csBuf != _T(""))	nNeedLic = _tstoi(csBuf);

				csArtNum = xChild.getChildNode(_T("ArtNum")).getText();

				prgWeb.nLangs = 0;
				prgWeb.nKeys = 0;
				prgWeb.csName = csName;
				prgWeb.nID = _tstoi(csID);
				prgWeb.nProgtype = nProgtype;
				prgWeb.nLicense = nNeedLic;
				prgWeb.csArtNum = csArtNum;
				prgWeb.nLangs = xChild.nChildNode(_T("Lang"));

				bFound = FALSE;

				nLangIndex = 0;
				for(nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)
				{
					prgLocal = theApp.m_caPrograms.GetAt(nLoop);

					if(prgLocal.nID == prgWeb.nID)	// same program?
					{
						bFound = TRUE;
						break;
					}

					nLangIndex += prgLocal.nLangs;
				}

				if(bFound == TRUE)	// we have found the program
				{
					if(prgWeb.nLangs != 0)	// we have some langs
					{
						for(j=0; j<prgWeb.nLangs; j++)
						{
							xLang = xChild.getChildNode(_T("Lang"), &jterator);

							if(j < prgLocal.nLangs)
								lng = theApp.m_caLanguages.GetAt(nLangIndex + j);
							else
							{
								lng.csPath = _T("");
								lng.csDesc = _T("");
								lng.nVersion = 0;
								lng.csVersion = _T("");
							}

							lng.csLang = xLang.getChildNode(_T("ID")).getText();
							lng.csVersionWeb = m_cLocale.FixLocale(xLang.getChildNode(_T("Version")).getText());
							lng.csPathWeb = xLang.getChildNode(_T("Filename")).getText();
							lng.csDesc = xLang.getChildNode(_T("Description")).getText();
							lng.csDate = xLang.getChildNode(_T("Date")).getText();

							if(j < prgLocal.nLangs)
								theApp.m_caLanguages.SetAt(nLangIndex + j, lng);
							else
								theApp.m_caLanguages.InsertAt(nLangIndex + j, lng);
						}

						jterator = 0;
					}

					prgLocal.nLangs = prgWeb.nLangs;

					// update the program
					theApp.m_caPrograms.SetAt(nLoop, prgLocal);
				}
				else if(bFound == FALSE)	// program doesn't exist in local file.
				{
					if(prgWeb.nLangs != 0)	// we have some langs
					{
						for(j=0; j<prgWeb.nLangs; j++)
						{
							xLang = xChild.getChildNode(_T("Lang"), &jterator);

							lng.csLang = xLang.getChildNode(_T("ID")).getText();
							lng.nVersion = 0;
							lng.csVersionWeb = m_cLocale.FixLocale(xLang.getChildNode(_T("Version")).getText());
							lng.csPath = _T("");
							lng.csPathWeb = xLang.getChildNode(_T("Filename")).getText();
							lng.csDesc = xLang.getChildNode(_T("Description")).getText();
							lng.csDate = xLang.getChildNode(_T("Date")).getText();
							lng.bLocal = FALSE;

							theApp.m_caLanguages.InsertAt(nLangIndex + j, lng);
						}

						jterator = 0;
					}

					// add it
					theApp.m_caPrograms.Add(prgWeb);
				}
			}
		}
	}

	return 0;
}

// used to get both the local and the remote (if possible) XML-files.
int CProgramsDlg::ParseXML()
{
	GetLocalXML();

	return TRUE;
}


int CProgramsDlg::SaveXML()
{
	CFile fh;
	TCHAR szBuf[1024];
	CTime time = CTime::GetCurrentTime();
	CString csDestpath;

	csDestpath = theApp.m_csLocalPath;
	CString csFile = csDestpath + _T("Programs.xml");

	if(fh.Open(csFile, CFile::modeCreate|CFile::modeWrite) != 0)
	{
		PROGRAM prg;
		KEY key;
		LANGUAGE lng;
		int nKey = 0, nLoop3, nLang=0;

#ifdef UNICODE
		/*const static BYTE UTF81 = 0xEF;	//0xfeff;
		const static BYTE UTF82 = 0xBB;	//0xfeff;
		const static BYTE UTF83 = 0xBF;
		fh.Write(&UTF81, 1);//﻿
		fh.Write(&UTF82, 1);
		fh.Write(&UTF83, 1);*/

		const static WORD UNICODE_MARK = 0xfeff;
		fh.Write(&UNICODE_MARK, 2);

		_stprintf(szBuf, _T("<?xml version=\"1.0\" encoding=\"UTF-16\"?>\r\n<Programs>\r\n\t<Path>%s</Path>\r\n\t<Version>%d</Version>\r\n"),
			csFile, m_nVersionXML);
		fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
#else
		sprintf(szBuf, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\r\n<Programs>\r\n\t<Path>%s</Path>\r\n\t<Version>%d</Version>\r\n",
			csFile, m_nVersionXML);
		fh.Write(szBuf, strlen(szBuf));
#endif

//		_stprintf(szBuf, _T("\t<Date>%04d-%02d-%02d %02d:%02d:%02d</Date>\r\n"), time.GetYear(), time.GetMonth(), time.GetDay(), time.GetHour(), time.GetMinute(), time.GetSecond());
		_stprintf(szBuf, _T("<Date>%04d-%02d-%02d %02d:%02d:%02d</Date>\r\n"), time.GetYear(), time.GetMonth(), time.GetDay(), time.GetHour(), time.GetMinute(), time.GetSecond());
#ifdef UNICODE
		fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
#else
		fh.Write(szBuf, strlen(szBuf));
#endif

		for(int nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)	// loop through all programs
		{
			prg = theApp.m_caPrograms.GetAt(nLoop);

//			_stprintf(szBuf, _T("\t<Product>\r\n\t\t<ID>%d</ID>\r\n\t\t<Name>%s</Name>\r\n\t\t<Progtype>%d</Progtype>\r\n\t\t<NeedLic>%d</NeedLic>\r\n"),
			_stprintf(szBuf, _T("<Product>\r\n<ID>%d</ID>\r\n<Name>%s</Name>\r\n<Progtype>%d</Progtype>\r\n<NeedLic>%d</NeedLic>\r\n<ArtNum>%s</ArtNum>\r\n"),
				prg.nID,
				prg.csName,
				prg.nProgtype,
				prg.nLicense,
				prg.csArtNum);
#ifdef UNICODE
			fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
#else
			fh.Write(szBuf, strlen(szBuf));
#endif

			if(prg.nLangs != 0)
			{
				for(nLoop3=nLang; nLoop3<(nLang+prg.nLangs); nLoop3++)
				{
					lng = theApp.m_caLanguages.GetAt(nLoop3);

//					_stprintf(szBuf, _T("\t\t<Lang>\r\n\t\t\t<ID>%s</ID>\r\n\t\t\t<Version>%s</Version>\r\n\t\t\t<Filename>%s</Filename>\r\n\t\t\t<Description>%s</Description>\r\n\t\t\t<Date>%s</Date>\r\n\t\t\t<Visible>%d</Visible>\r\n\t\t</Lang>\r\n"),
					_stprintf(szBuf, _T("<Lang>\r\n<ID>%s</ID>\r\n<Version>%s</Version>\r\n<Filename>%s</Filename>\r\n<Description>%s</Description>\r\n<Date>%s</Date>\r\n<Visible>%d</Visible>\r\n</Lang>\r\n"),
								   lng.csLang,
								   lng.csVersion,
								   lng.csPath,
								   lng.csDesc,
								   lng.csDate,
								   lng.bVisible);

#ifdef UNICODE
					fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
#else
					fh.Write(szBuf, strlen(szBuf));
#endif
				}

				nLang += prg.nLangs;
			}

//			_stprintf(szBuf, _T("\t</Product>\r\n"));
			_stprintf(szBuf, _T("</Product>\r\n"));
#ifdef UNICODE
			fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
#else
			fh.Write(szBuf, strlen(szBuf));
#endif
		}

		_stprintf(szBuf, _T("</Programs>\r\n"));
#ifdef UNICODE
		fh.Write(szBuf, _tcslen(szBuf)*sizeof(TCHAR));
#else
		fh.Write(szBuf, strlen(szBuf));
#endif

		fh.Close();
	}
	else
	{
		return -1;
	}

	return 0;
}

// create a new package
void CProgramsDlg::OnNew()
{
	// open the info-dialog
	CDocTemplate *pTemplate;
	CWinApp* pApp = AfxGetApp();
	CString csDocName, csResStr;
	csResStr.LoadString(IDR_PROGRAM);

	CMDISampleSuiteDoc* pDocument;
	POSITION posDOC;
	POSITION pos;

	pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(csDocName, CDocTemplate::docName);
		ASSERT(pTemplate != NULL);
		// Need to add a linefeed, infront of the docName.
		// This is because, for some reason, the document title,
		// set in resource, must have a linefeed.
		// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
		csDocName = '\n' + csDocName;

		if (pTemplate && csDocName.Compare(csResStr) == 0)
		{
			posDOC = pTemplate->GetFirstDocPosition();

			while(posDOC != NULL)
			{
				pDocument = (CMDISampleSuiteDoc*)pTemplate->GetNextDoc(posDOC);
				pos = pDocument->GetFirstViewPosition();
				if(pos != NULL && pDocument->m_nType == pDoc->m_nType)
				{
					pDocument->m_bNew = TRUE;
					pDocument->m_bClick = TRUE;
					CView* pView = pDocument->GetNextView(pos);
					pView->GetParent()->BringWindowToTop();
					pView->GetParent()->SetFocus();
//					pView->GetDocument();
					posDOC = (POSITION)1;
					break;
				}
			}

			if(posDOC == NULL)
			{
				theApp.m_nType = pDoc->m_nType;
				theApp.m_bNew = TRUE;
				theApp.m_bClick = TRUE;
				pDocument = (CMDISampleSuiteDoc*)pTemplate->OpenDocumentFile(NULL);
				pDocument->SetTitle(g_pXML->str(1000));
				pDocument->m_nType = pDoc->m_nType;
//				pDocument->UpdateAllViews(NULL);
			}
		}
	}
}

void CProgramsDlg::OnSetFocus(CWnd* pOldWnd)
{
//	CXTResizeFormView::OnSetFocus(pOldWnd);
	CView::OnSetFocus(pOldWnd);

	GetReportCtrl().SetFocus();

	// turn off all imagebuttons in the main window
	if(((CProgramsFrame*)GetParent())->m_bLock == FALSE && theApp.m_bWriteAllowed == TRUE)
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, TRUE);
	else
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
}

// delete a package
void CProgramsDlg::OnDelete()
{
	PROGRAM prg;
	LANGUAGE lng;
	CString csName, csLang;
	CString csVersion;
	int nLoop, nLoop2, nLangIndex=0, nLang, nProg;
	BOOL bFoundPrg=FALSE, bFoundLang=FALSE;

	CMessageRecord* pRecord = DYNAMIC_DOWNCAST(CMessageRecord, GetReportCtrl().GetFocusedRow()->GetRecord());
	if (pRecord)
	{
		int nIndex = pRecord->GetIndex();

		CXTPReportRecordItem* pItem = pRecord->GetItem(COLUMN_PROGRAM);	// name
		csName = pItem->GetCaption(0);

		pItem = pRecord->GetItem(COLUMN_VERSION);	// version
//		nVersion = (atof(m_cLocale.FixLocale(pItem->GetCaption(0))) * 10.0);
		csVersion = pItem->GetCaption(0);
		csVersion = csVersion.Left(csVersion.Find(_T(" ("), 0));

		pItem = pRecord->GetItem(COLUMN_TLA);	// language
		csLang = pItem->GetCaption(0);

		// search for the program
		for(nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)
		{
			prg = theApp.m_caPrograms.GetAt(nLoop);

			if(prg.nID == pRecord->m_nProgID &&
				prg.csName == csName)
			{
				// search for the language
				for(nLoop2=0; nLoop2<prg.nLangs; nLoop2++)
				{
					lng = theApp.m_caLanguages.GetAt(nLangIndex + nLoop2);

					if(lng.csVersion == csVersion &&	//lng.nVersion == nVersion &&
						lng.csLang == csLang)
					{
						nLang = nLangIndex + nLoop2;
						bFoundLang = TRUE;
						break;
					}
				}

				nProg = nLoop;
				bFoundPrg = TRUE;
				break;
			}

			nLangIndex += prg.nLangs;
		}

		if(bFoundPrg == FALSE || bFoundLang == FALSE) return;

		
		// delete the package
		CString csPath;
		csPath.Format(_T("%s%d%s.hmi"), theApp.m_csLocalPath, prg.nID,  lng.csLang);
		DeleteFile(csPath);

		// remove entries from the arrays
		theApp.m_caPrograms.RemoveAt(nProg);
		theApp.m_caLanguages.RemoveAt(nLang);

		// remove the object from the reportcontrol
		GetReportCtrl().GetRecords()->RemoveAt(nIndex);
		GetReportCtrl().Populate();

		SaveXML();
	}
}

// show the manual for this program
void CProgramsDlg::OnBnClickedManual()
{
	PROGRAM prg;
	LANGUAGE lng;
	CString csName, csLang;
	CString csVersion;
	int nLoop, nLoop2, nLangIndex=0, nLang, nProg;
	BOOL bFoundPrg=FALSE, bFoundLang=FALSE;

	CMessageRecord* pRecord = DYNAMIC_DOWNCAST(CMessageRecord, GetReportCtrl().GetFocusedRow()->GetRecord());
	if (pRecord)
	{
		int nIndex = pRecord->GetIndex();

		CXTPReportRecordItem* pItem = pRecord->GetItem(COLUMN_PROGRAM);	// name
		csName = pItem->GetCaption(0);

		pItem = pRecord->GetItem(COLUMN_VERSION);	// version
//		nVersion = (atof(m_cLocale.FixLocale(pItem->GetCaption(0))) * 10.0);
		csVersion = pItem->GetCaption(0);
		csVersion = csVersion.Left(csVersion.Find(_T(" ("), 0));

		pItem = pRecord->GetItem(COLUMN_TLA);	// language
		csLang = pItem->GetCaption(0);

		// search for the program
		for(nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)
		{
			prg = theApp.m_caPrograms.GetAt(nLoop);

			if(prg.nID == pRecord->m_nProgID &&
				prg.csName == csName)
			{
				// search for the language
				for(nLoop2=0; nLoop2<prg.nLangs; nLoop2++)
				{
					lng = theApp.m_caLanguages.GetAt(nLangIndex + nLoop2);

					if(lng.csVersion == csVersion &&	//lng.nVersion == nVersion &&
						lng.csLang == csLang)
					{
						nLang = nLangIndex + nLoop2;
						bFoundLang = TRUE;
						break;
					}
				}

				nProg = nLoop;
				bFoundPrg = TRUE;
				break;
			}

			nLangIndex += prg.nLangs;
		}

		if(bFoundPrg == FALSE || bFoundLang == FALSE) return;


		// get the program-file from the HMI
		CString csBuf, csFilename;

		csBuf = lng.csPath.Right(4);
		if(csBuf.CompareNoCase(_T(".hmi")) == 0)
		{
			CString csDestpath = theApp.m_csLocalPath;

			g_pZIP->Open(csDestpath /*+ _T("\\")*/ + lng.csPath, CZipArchive::zipOpenReadOnly);

			// extract "package.xml" and inspect it
			int nIndex = -1, nType=0;
			nIndex = g_pZIP->FindFile(_T("package.xml"), CZipArchive::ffNoCaseSens, true);

			// we got a package.xml?
			if(nIndex != -1)
			{
				g_pZIP->ExtractFile(nIndex, theApp.m_csTemppath);

				// parse the package xml-file
				XMLNode xNode = XMLNode::openFileHelper(theApp.m_csTemppath + _T("package.xml"), _T("XML"));
				CArray<int, int> caDocsIndex;
				CArray<CString, CString> caDocsFiles;

				if(xNode.isEmpty() != 1)
				{
					xNode = xNode.getChildNode(_T("package"));	// get the first tag, "package"
					if(xNode.isEmpty() != 1)
					{
						XMLNode xChild;
						CString csBuf, csFile;
						int i, iterator=0;
						BOOL bDoc = FALSE;


						int n = xNode.nChildNode(_T("file"));
						for(i=0; i<n; i++)
						{
							xChild = xNode.getChildNode(_T("file"), &iterator);

							nIndex = -1;
							csFile = xChild.getAttribute(_T("name"));
							nIndex = g_pZIP->FindFile(csFile, CZipArchive::ffNoCaseSens, true);

							if(nIndex != -1)
							{
								csBuf = xChild.getAttribute(_T("type"));
								if(csBuf == _T("DP")) nType = 1;
								else if(csBuf == _T("DPDOC")) nType = 2;
								else if(csBuf == _T("GENERIC")) nType = 3;

								if(nType == 2)
								{
									caDocsIndex.Add(nIndex);
									caDocsFiles.Add(csFile);
								}
							}
						}
					}
				}

				// delete the xml-file
				DeleteFile(theApp.m_csTemppath + _T("package.xml"));


				// display the document
				if(caDocsIndex.GetSize() > 1)
				{
					// ask about which document to show
					// display a popup-menu with the different alternatives (documents).
					POINT point;
					CMenu menuPopup;
					menuPopup.CreatePopupMenu();
					
					// add the files to the popup-menu
					for(int nLoop=0; nLoop<caDocsIndex.GetSize(); nLoop++)
					{
						menuPopup.AppendMenu(MF_STRING, nLoop+1, (LPCTSTR)caDocsFiles.GetAt(nLoop));
					}

					GetCursorPos(&point);
					int nRet = ::TrackPopupMenu(menuPopup.GetSafeHmenu(), TPM_NONOTIFY|TPM_RETURNCMD|TPM_LEFTALIGN|TPM_RIGHTBUTTON, point.x, point.y, NULL, this->GetSafeHwnd(), NULL);
					menuPopup.DestroyMenu();

					// unpack the programfile and break
					if(nRet == 0) return;

					csFilename.Format(_T("%s%s"), theApp.m_csTemppath, caDocsFiles.GetAt(nRet-1));
					g_pZIP->ExtractFile(caDocsIndex.GetAt(nRet-1), theApp.m_csTemppath);
				}
				else
				{
					// unpack the programfile and break
					csFilename.Format(_T("%s%s"), theApp.m_csTemppath, caDocsFiles.GetAt(0));
					g_pZIP->ExtractFile(caDocsIndex.GetAt(0), theApp.m_csTemppath);
				}

				g_pZIP->Close();


				// tell the Communication-suite to send the file
				if(nIndex != -1 && nType == 2)
				{
					ShellExecute(this->m_hWnd, NULL, csFilename, NULL, NULL, SW_SHOWNORMAL);
					return;

					CString csFile;
					csFile.Format(_T("explorer.exe \"%s\""), csFilename);
					STARTUPINFO si;
					PROCESS_INFORMATION pi;

					ZeroMemory( &si, sizeof(si) );
					si.cb = sizeof(si);
					ZeroMemory( &pi, sizeof(pi) );

					if( !CreateProcess( NULL, // No module name (use command line). 
						csFile.GetBuffer(0), // Command line. 
						NULL,             // Process handle not inheritable. 
						NULL,             // Thread handle not inheritable. 
						FALSE,            // Set handle inheritance to FALSE. 
						0,                // No creation flags. 
						NULL,             // Use parent's environment block. 
						NULL,             // Use parent's starting directory. 
						&si,              // Pointer to STARTUPINFO structure.
						&pi )             // Pointer to PROCESS_INFORMATION structure.
						) 
					{
						MessageBox(_T("Error opening explorer.exe!"), _T("Error"), MB_ICONSTOP);
					}

//					DeleteFile(csFilename);
				}
				else
				{
					MessageBox(_T("There is no document in this package!"), _T("Error"), MB_ICONSTOP);
				}
			}
			else
				g_pZIP->Close();
		}
	}
}

// show the licensekeys for the current selected program.
void CProgramsDlg::OnBnClickedLicense()
{
	OnReportItemDblClick(0, 0);
}

// download/update program button is clicked.
void CProgramsDlg::OnBnClickedDownload()
{
	// check if we have a valid license.
	if( theApp.CheckLicense(_T("H2020B")) == FALSE)
		return;


	CString csSerial, csLevel;

	// ask the user about serialnumber of the unit
	CSerialDlg dlg;
	int nRet = dlg.DoModal();
	if(nRet != IDOK || dlg.m_csSerial == _T(""))
	{
		return;
	}
	csSerial = dlg.m_csSerial;
	csLevel = dlg.m_csLevel;

	m_licenses.clear();

	CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
	for( int i = 0; i < pRecs->GetCount() - 1; i++ ) // Swedish only
	{
		CMessageRecord* pRecord = DYNAMIC_DOWNCAST(CMessageRecord, pRecs->GetAt(i));
		if( pRecord && pRecord->GetItem(COLUMN_CHECK)->IsChecked() && CString(pRecord->GetItem(COLUMN_TLA)->GetCaption(0)).Compare(_T("SVE")) == 0 )
		{
			DownloadPackage(csSerial, csLevel, pRecord);
		}
	}
	for( int i = 0; i < pRecs->GetCount() - 1; i++ ) // Other languages
	{
		CMessageRecord* pRecord = DYNAMIC_DOWNCAST(CMessageRecord, pRecs->GetAt(i));
		if( pRecord && pRecord->GetItem(COLUMN_CHECK)->IsChecked() && CString(pRecord->GetItem(COLUMN_TLA)->GetCaption(0)).Compare(_T("SVE")) != 0 )
		{
			DownloadPackage(csSerial, csLevel, pRecord);
		}
	}

	// Show license documents
	GenerateLicenseReports();
}

void CProgramsDlg::DownloadPackage(CString csSerial, CString csLevel, CMessageRecord* pRecord)
{
	PROGRAM prg;
	LANGUAGE lng;
	CString csName, csLang;
	CString csVersion;
	int nLoop, nLoop2, nLangIndex=0, nLang, nProg;
	BOOL bFoundPrg=FALSE, bFoundLang=FALSE;
	
	CXTPReportRecordItem* pItem = pRecord->GetItem(COLUMN_PROGRAM);	// name
	csName = pItem->GetCaption(0);

	pItem = pRecord->GetItem(COLUMN_VERSION);	// version
//		nVersion = (atof(m_cLocale.FixLocale(pItem->GetCaption(0))) * 10.0);
	csVersion = pItem->GetCaption(0);
	csVersion = csVersion.Left(csVersion.Find(_T(" ("), 0));


	pItem = pRecord->GetItem(COLUMN_TLA);	// language
	csLang = pItem->GetCaption(0);

	// search for the program
	for(nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)
	{
		prg = theApp.m_caPrograms.GetAt(nLoop);

		if(prg.nID == pRecord->m_nProgID &&
			prg.csName == csName)
		{
			// search for the language
			for(nLoop2=0; nLoop2<prg.nLangs; nLoop2++)
			{
				lng = theApp.m_caLanguages.GetAt(nLangIndex + nLoop2);

				if(lng.csVersion == csVersion &&	//lng.nVersion == nVersion &&
					lng.csLang == csLang)
				{
					nLang = nLangIndex + nLoop2;
					bFoundLang = TRUE;
					break;
				}
			}

			nProg = nLoop;
			bFoundPrg = TRUE;
			break;
		}

		nLangIndex += prg.nLangs;
	}

	if(bFoundPrg == FALSE || bFoundLang == FALSE) return;

	// get the program-file from the HMI
	CString csBuf, csFilename;

	csBuf = lng.csPath.Right(4);
	if(csBuf.CompareNoCase(_T(".hmi")) == 0)
	{
		CString csDestpath = theApp.m_csLocalPath;

		g_pZIP->Open(csDestpath /*+ _T("\\")*/ + lng.csPath, CZipArchive::zipOpenReadOnly);

		// extract "package.xml" and inspect it
		int nIndex = -1, nType=0;
		nIndex = g_pZIP->FindFile(_T("package.xml"), CZipArchive::ffNoCaseSens, true);

		// we got a package.xml?
		if(nIndex != -1)
		{
			g_pZIP->ExtractFile(nIndex, theApp.m_csTemppath);

			// parse the package xml-file
			XMLNode xNode = XMLNode::openFileHelper(theApp.m_csTemppath + _T("package.xml"), _T("XML"));

			if(xNode.isEmpty() != 1)
			{
				xNode = xNode.getChildNode(_T("package"));	// get the first tag, "package"
				if(xNode.isEmpty() != 1)
				{
					XMLNode xChild;
					CString csBuf, csFile;
					int i, iterator=0;
					BOOL bDoc = FALSE;

					int n = xNode.nChildNode(_T("file"));
					for(i=0; i<n; i++)
					{
						xChild = xNode.getChildNode(_T("file"), &iterator);

						nIndex = -1;
						csFile = xChild.getAttribute(_T("name"));
						nIndex = g_pZIP->FindFile(csFile, CZipArchive::ffNoCaseSens, true);

						if(nIndex != -1)
						{
							csBuf = xChild.getAttribute(_T("type"));
							if(csBuf == _T("DP")) nType = 1;
							else if(csBuf == _T("DPDOC")) nType = 2;
							else if(csBuf == _T("GENERIC")) nType = 3;

							if(nType == 1)
							{
								// unpack the programfile and break
								csFilename.Format(_T("%s%s"), theApp.m_csTemppath, csFile);
								g_pZIP->ExtractFile(nIndex, theApp.m_csTemppath);

								break;
							}
						}
					}
				}
			}

			g_pZIP->Close();

			// delete the xml-file
			DeleteFile(theApp.m_csTemppath + _T("package.xml"));

			if(nIndex != -1 && nType == 1)
			{
// -----------------------------
				static int a, b;
				unsigned char p[8];

				b = prg.nID + (_tstoi(csLevel)-1);
				a = _tstoi(csSerial);
				memcpy(&p,(void*)&a, 4);
				memcpy(p+4,(char *)&b, 4);

				unsigned char *z;
				z = p;

				int i;
				unsigned short zlr;
				static unsigned int crc, g;
				crc=0;

				g = (0x01021 << 8) & 0x00ffff00;

				for(i=0; i<8; i++)
				{
					crc |= ((long) (*z++) & 0x000000ff);

					if(i == 8-2) i = 8-2;

					for(zlr=0; zlr<8; zlr++)
					{
						crc <<= 1;
						if ((crc & 0x01000000) != 0) crc ^= g;
					}
				}

				crc = ((unsigned short)((crc & 0x00ffff00) >> 8));
// -----------------------------
				csBuf.Format(_T("%s;%d;%s;%d;"), csSerial, crc, csLevel, prg.nID);

				// tell the Communication-suite to send the file
				AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&_user_msg(401, _T("OpenSuiteEx"), _T("Communication.dll"), csBuf, csFilename, _T("TRUE")));

				// store license data
				LICENSEDATA ld;
				ld.serial = csSerial;
				ld.name = prg.csName;
				ld.version = lng.csVersion;
				ld.code = crc;
				m_licenses[prg.nID] = ld;
			}
			else
			{
				MessageBox(_T("There is no program in this package!"), _T("Error"), MB_ICONSTOP);
			}
		}
		else
			g_pZIP->Close();
	}
}

void CProgramsDlg::GenerateLicenseReports()
{
#ifdef MYDEBUG
	//don't show reports
	//return;
#endif

	// Request lang, tech info from user
	CLicenseReportDialog dlg;
	dlg.DoModal();

	std::map<int, LICENSEDATA>::const_iterator iter = m_licenses.begin();

	while( iter != m_licenses.end() )
	{
		LICENSEDATA ld = iter->second;

		if(ld.name == _T("") || ld.version == _T(""))
		{
			//open dialog to get program name and version from user
			CExtraLicenseReportInfoDlg dialog;
			dialog.m_csFile = ld.filename;
			dialog.m_csName = ld.name;
			dialog.m_csVersion = ld.version;

			if(dialog.DoModal() != IDOK)
				continue;

			ld.name = dialog.m_csName;
			ld.version = dialog.m_csVersion;
		}

		// open that file in the reportviewer
		CString csBuf;
		csBuf.Format(_T("%s;%s;%s;%d;%d;%s"), ld.serial, ld.name, ld.version, iter->first, ld.code, dlg.csTech);

		// generate both ENU and SVE report
		CString csReport;
		csReport.Format(_T("%sSVE\\dp_licensSVE.fr3"), getReportsDir());
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&_user_msg(300, _T("OpenSuiteEx"), _T("Reports.dll"), csReport, _T(""), csBuf));

		csReport.Format(_T("%sENU\\dp_licensENU.fr3"), getReportsDir());
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&_user_msg(300, _T("OpenSuiteEx"), _T("Reports.dll"), csReport, _T(""), csBuf));

		iter++;
	}
}


//uncheck all programs
void CProgramsDlg::OnBnClickedRefresh()
{
	CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
	if(!pRecs)
		return;

	for( int i = 0; i < pRecs->GetCount() - 1; i++ ) 
	{
		if(pRecs->GetAt(i))
			pRecs->GetAt(i)->GetItem(COLUMN_CHECK)->SetChecked(FALSE);

	}

	GetReportCtrl().RedrawControl();
}

void CProgramsDlg::OnBnClickedSelectFiles()
{
	int nID = -1;

	// check if we have a valid license.
	if( theApp.CheckLicense(_T("H2020B")) == FALSE)
		return;

	CString csSerial, csLevel;
	CString buf_filename;
	
	// ask the user about serialnumber of the unit
	CSerialDlg dlg;
	int nRet = dlg.DoModal();
	if(nRet != IDOK || dlg.m_csSerial == _T(""))
	{
		return;
	}
	csSerial = dlg.m_csSerial;
	csLevel = dlg.m_csLevel;

	m_licenses.clear();

	//open file dialog
	CFileDialog *pOpenFileDlg = new CFileDialog(TRUE);

	pOpenFileDlg->GetOFN().lpstrTitle = _T("Select files to send");	//csTitle;

	pOpenFileDlg->GetOFN().lpstrFile = buf_filename.GetBuffer(2048*(_MAX_PATH + 1) +1);
	pOpenFileDlg->GetOFN().nMaxFile = 2048;
	
	if(!pOpenFileDlg)
	{
		return;
	}

	pOpenFileDlg->m_ofn.lpstrFilter = _T("Digitech Pro (*.m$x)\0*.m$x\0");
	pOpenFileDlg->m_ofn.Flags = OFN_ALLOWMULTISELECT|OFN_EXPLORER|OFN_HIDEREADONLY;

	if(pOpenFileDlg->DoModal() != IDOK)
	{
		buf_filename.ReleaseBuffer();
		delete pOpenFileDlg;
		return;
	}

	POSITION pos;
	CString csFN, csFileName = _T(""), csBuf;

	pos = pOpenFileDlg->GetStartPosition();
	while(pos)
	{
		csFileName = pOpenFileDlg->GetNextPathName(pos);
		csFN = csFileName.Right(csFileName.GetLength() - csFileName.ReverseFind('\\') - 1);

		//get Product ID from file
		nID = getProductCode(csFileName);
		
		if(nID != -1)
		{
			//calculate dp license code
			static int a, b;
			unsigned char p[8];

			b = nID + (_tstoi(csLevel)-1);
			a = _tstoi(csSerial);
			memcpy(&p,(void*)&a, 4);
			memcpy(p+4,(char *)&b, 4);

			unsigned char *z;
			z = p;

			int i;
			unsigned short zlr;
			static unsigned int crc, g;
			crc=0;

			g = (0x01021 << 8) & 0x00ffff00;

			for(i=0; i<8; i++)
			{
				crc |= ((long) (*z++) & 0x000000ff);

				if(i == 8-2) i = 8-2;

				for(zlr=0; zlr<8; zlr++)
				{
					crc <<= 1;
					if ((crc & 0x01000000) != 0) crc ^= g;
				}
			}

			crc = ((unsigned short)((crc & 0x00ffff00) >> 8));
			// -----------------------------
			csBuf.Format(_T("%s;%d;%s;%d;"), csSerial, crc, csLevel, nID);

			// tell the Communication-suite to send the file
			if(csFileName != _T(""))
				AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&_user_msg(401, _T("OpenSuiteEx"), _T("Communication.dll"), csBuf, csFileName, _T("TRUE")));

			// store license data

			LICENSEDATA ld;
			ld.serial = csSerial;
			ld.name = _T("");
			ld.version = _T("");
			ld.code = crc;
			ld.filename = csFileName;
			m_licenses[nID] = ld;
		}
		else
		{
			AfxMessageBox(_T("Can't extract Product ID from file"), MB_ICONERROR);
		}
	}

	buf_filename.ReleaseBuffer();
	delete pOpenFileDlg;

	// Show license documents
	GenerateLicenseReports();

}

//added #3576
void CProgramsDlg::OnBnClickedSendToDP2()
{
	int nID = -1;

	// check if we have a valid license.
	if( theApp.CheckLicense(_T("H2020B")) == FALSE)
		return;

	CString csSerial, csLevel;
	CString buf_filename;
	CString csDest = _T("");
	
	// ask the user about serialnumber of the unit
	CSerialDlg dlg;
	int nRet = dlg.DoModal();
	if(nRet != IDOK || dlg.m_csSerial == _T(""))
	{
		return;
	}
	csSerial = dlg.m_csSerial;
	csLevel = dlg.m_csLevel;

	m_licenses.clear();

	//open file dialog
	CFileDialog *pOpenFileDlg = new CFileDialog(TRUE);

	pOpenFileDlg->GetOFN().lpstrTitle = _T("Select files to send");	//csTitle;

	pOpenFileDlg->GetOFN().lpstrFile = buf_filename.GetBuffer(2048*(_MAX_PATH + 1) +1);
	pOpenFileDlg->GetOFN().nMaxFile = 2048;
	
	if(!pOpenFileDlg)
	{
		return;
	}

	pOpenFileDlg->m_ofn.lpstrFilter = _T("Digitech Pro II (*.dp2)\0*.dp2\0");
	pOpenFileDlg->m_ofn.Flags = OFN_ALLOWMULTISELECT|OFN_EXPLORER|OFN_HIDEREADONLY;

	if(pOpenFileDlg->DoModal() != IDOK)
	{
		buf_filename.ReleaseBuffer();
		delete pOpenFileDlg;
		return;
	}

	POSITION pos;
	CString csFN, csFileName = _T(""), csBuf;

	pos = pOpenFileDlg->GetStartPosition();

	//#3756 om filer valts s� k�r upp v�lj destinationsruta
	if(pos >(POSITION)0)
	{
		//h�mta senast sparade s�kv�gen fr�n registret
		csDest =  regGetStr(REG_ROOT, _T("\\HMS_Development\\Settings"), _T("LastPathDP2"), _T(""));

		TCHAR szFolder[MAX_PATH*2];
		szFolder[0] = _T('\0');
		BOOL bRet = XBrowseForFolder(this->GetSafeHwnd(),
				csDest,
				szFolder,
				sizeof(szFolder)/sizeof(TCHAR)-2);

		if(bRet == TRUE)
		{
			csDest = szFolder;
			if(csDest.GetAt(csDest.GetLength()-1) != '\\')
					csDest += _T("\\");

			//spara ner senaste s�kv�gen till registret
			regSetStr(REG_ROOT, _T("\\HMS_Development\\Settings"), _T("LastPathDP2"), csDest);
		}
		else
		{
			buf_filename.ReleaseBuffer();
			delete pOpenFileDlg;
			return;
		}
	}

	while(pos)
	{
		csFileName = pOpenFileDlg->GetNextPathName(pos);
		csFN = csFileName.Right(csFileName.GetLength() - csFileName.ReverseFind('\\') - 1);

		//get Product ID from file
		nID = getProductCode(csFileName);
		
		if(nID != -1)
		{
			//calculate dp license code
			static int a, b;
			unsigned char p[8];

			b = nID + (_tstoi(csLevel)-1);
			a = _tstoi(csSerial);
			memcpy(&p,(void*)&a, 4);
			memcpy(p+4,(char *)&b, 4);

			unsigned char *z;
			z = p;

			int i;
			unsigned short zlr;
			static unsigned int crc, g;
			crc=0;

			g = (0x01021 << 8) & 0x00ffff00;

			for(i=0; i<8; i++)
			{
				crc |= ((long) (*z++) & 0x000000ff);

				if(i == 8-2) i = 8-2;

				for(zlr=0; zlr<8; zlr++)
				{
					crc <<= 1;
					if ((crc & 0x01000000) != 0) crc ^= g;
				}
			}

			crc = ((unsigned short)((crc & 0x00ffff00) >> 8));
			// -----------------------------
			csBuf.Format(_T("%s;%d;%s;%d;"), csSerial, crc, csLevel, nID);

			//#3756 kopia fil till destination
			copyFileDP2(csDest, csFileName, csFN, _tstoi(csSerial), crc, _tstoi(csLevel), nID);
			
			// store license data

			LICENSEDATA ld;
			ld.serial = csSerial;
			ld.name = _T("");
			ld.version = _T("");
			ld.code = crc;
			ld.filename = csFileName;
			m_licenses[nID] = ld;
		}
		else
		{
			AfxMessageBox(_T("Can't extract Product ID from file"), MB_ICONERROR);
		}
	}

	buf_filename.ReleaseBuffer();
	delete pOpenFileDlg;

	// Show license documents
	GenerateLicenseReports();

}

bool CProgramsDlg::copyFileDP2(CString dest, CString org, CString filename, int m_nSerial, int m_nCode, int m_nLevel, int m_nProd)
{
	bool bRet = true;
	CString csTmpFN, csDate,csMsg;
char *szFileBuf;

	csDate = getDateTime();
	csDate.Remove('-');
	csDate.Remove(':');
	csDate.Remove(' ');
	csTmpFN = getMyDocumentsDir() + _T("\\");
	csTmpFN += filename;
	csTmpFN += csDate;

	//kopiera till tempor�r fil, l�gg p� tidsst�mpel till filnamnet f�r att g�ra unikt
	if(CopyFile(org,csTmpFN, FALSE) == 0)
	{
		csMsg.Format(_T("Failed to copy file\r\n%s\r\nto\r\n%s"),org,csTmpFN);
		AfxMessageBox(csMsg);
		return false;
	}

	//modifiera filen med lic kod
	CFile fh;
	if(!fh.Open(csTmpFN, CFile::modeReadWrite))
	{
		csMsg.Format(_T("Failed to open file\r\n%s"),csTmpFN);
		AfxMessageBox(csMsg);
		return false;
	}

	ULONG dwFilesize = fh.GetLength();
	szFileBuf = (char *)malloc(dwFilesize+1);
	if(szFileBuf == NULL )	// check that the malloc was sucessfull.
	{
		csMsg.Format(_T("Failed to allocate buffer"));
		AfxMessageBox(csMsg);
		fh.Close();
		return false;
	}

	// read the file into a temporar buffer
	fh.Read(szFileBuf, dwFilesize);

	// fix the code into the buffer.
	if(m_nSerial != -1 && m_nCode != -1)
	{
		m_nLevel--;

		if(CheckCode(m_nProd, m_nSerial, m_nLevel, m_nCode))
		{
			prg_struct_type prg;
			memset(&prg, 0, sizeof(prg_struct_type));
			memcpy(&prg, szFileBuf, sizeof(prg_struct_type));

			prg.productcode = prg.productcode + (m_nLevel << 28);	// put the level into the productcode
			prg.licenstart = m_nSerial;
			prg.licensend = m_nSerial;
			prg.checksum_ = crc16_typ1((unsigned char*)&prg, sizeof(prg_struct_type)-4);	//#3756 �ndrad fr�n typ2

			memcpy(szFileBuf, &prg, sizeof(prg_struct_type));
		}
	}
	else
	{
		csMsg.Format(_T("Wrong ID"));
		AfxMessageBox(csMsg);
		fh.Close();
		free(szFileBuf);
		return false;
	}

	fh.SeekToBegin();
	fh.Write(szFileBuf, dwFilesize);
	fh.Flush();
	fh.Close();
	free(szFileBuf);


	//flytta till dp2, �ndra filnamnet
	dest += filename;
	if(MoveFileEx(csTmpFN,dest,MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING) == 0)
	{
		csMsg.Format(_T("Failed to copy file to DPII"));
		AfxMessageBox(csMsg);
		return false;
	}

	return bRet;
}

int CProgramsDlg::getProductCode(CString filename)
{
	CFile cfFile;
	BYTE buf[4];
	int nBytes = 0;

	if(cfFile.Open(filename, CFile::modeRead) == 0)
		return -1;

	cfFile.Seek(12, CFile::begin);
	nBytes = cfFile.Read(buf,4);

	cfFile.Close();

	if(nBytes != 4)
		return -1;

	int result = 0;
	for (int n = 3; n >= 0; n--)
		result = (result << 8) + buf[ n ];

	return result;
}


//gjort om rutinen s� att det fungerar f�r dp2+, produktkoden ligger p� annat st�lle i filen
void CProgramsDlg::OnBnClickedSendToDP3()
{
	int nID = -1;

	// check if we have a valid license.
	if( theApp.CheckLicense(_T("H2020B")) == FALSE)
		return;

	CString csSerial, csLevel;
	CString buf_filename;
	CString csDest = _T("");
	
	// ask the user about serialnumber of the unit
	CSerialDlg dlg;
	int nRet = dlg.DoModal();
	if(nRet != IDOK || dlg.m_csSerial == _T(""))
	{
		return;
	}
	csSerial = dlg.m_csSerial;
	csLevel = dlg.m_csLevel;

	m_licenses.clear();

	//open file dialog
	CFileDialog *pOpenFileDlg = new CFileDialog(TRUE);

	pOpenFileDlg->GetOFN().lpstrTitle = _T("Select files to send");	//csTitle;

	pOpenFileDlg->GetOFN().lpstrFile = buf_filename.GetBuffer(2048*(_MAX_PATH + 1) +1);
	pOpenFileDlg->GetOFN().nMaxFile = 2048;
	
	if(!pOpenFileDlg)
	{
		return;
	}

	pOpenFileDlg->m_ofn.lpstrFilter = _T("Digitech Pro II+ (*.dp2+)\0*.dp2+\0");
	pOpenFileDlg->m_ofn.Flags = OFN_ALLOWMULTISELECT|OFN_EXPLORER|OFN_HIDEREADONLY;

	if(pOpenFileDlg->DoModal() != IDOK)
	{
		buf_filename.ReleaseBuffer();
		delete pOpenFileDlg;
		return;
	}

	POSITION pos;
	CString csFN, csFileName = _T(""), csBuf;

	pos = pOpenFileDlg->GetStartPosition();

	//om filer valts s� k�r upp v�lj destinationsruta
	if(pos >(POSITION)0)
	{
		//h�mta senast sparade s�kv�gen fr�n registret
		csDest =  regGetStr(REG_ROOT, _T("\\HMS_Development\\Settings"), _T("LastPathDP3"), _T(""));

		TCHAR szFolder[MAX_PATH*2];
		szFolder[0] = _T('\0');
		BOOL bRet = XBrowseForFolder(this->GetSafeHwnd(),
				csDest,
				szFolder,
				sizeof(szFolder)/sizeof(TCHAR)-2);

		if(bRet == TRUE)
		{
			csDest = szFolder;
			if(csDest.GetAt(csDest.GetLength()-1) != '\\')
					csDest += _T("\\");

			//spara ner senaste s�kv�gen till registret
			regSetStr(REG_ROOT, _T("\\HMS_Development\\Settings"), _T("LastPathDP3"), csDest);
		}
		else
		{
			buf_filename.ReleaseBuffer();
			delete pOpenFileDlg;
			return;
		}
	}

	while(pos)
	{
		csFileName = pOpenFileDlg->GetNextPathName(pos);
		csFN = csFileName.Right(csFileName.GetLength() - csFileName.ReverseFind('\\') - 1);

		//get Product ID from file
		nID = getProductCodeDP3(csFileName);
		
		if(nID != -1)
		{
			//calculate dp license code
			static int a, b;
			unsigned char p[8];

			b = nID + (_tstoi(csLevel)-1);
			a = _tstoi(csSerial);
			memcpy(&p,(void*)&a, 4);
			memcpy(p+4,(char *)&b, 4);

			unsigned char *z;
			z = p;

			int i;
			unsigned short zlr;
			static unsigned int crc, g;
			crc=0;

			g = (0x01021 << 8) & 0x00ffff00;

			for(i=0; i<8; i++)
			{
				crc |= ((long) (*z++) & 0x000000ff);

				if(i == 8-2) i = 8-2;

				for(zlr=0; zlr<8; zlr++)
				{
					crc <<= 1;
					if ((crc & 0x01000000) != 0) crc ^= g;
				}
			}

			crc = ((unsigned short)((crc & 0x00ffff00) >> 8));
			// -----------------------------
			csBuf.Format(_T("%s;%d;%s;%d;"), csSerial, crc, csLevel, nID);

			//#3756 kopia fil till destination
			copyFileDP3(csDest, csFileName, csFN, _tstoi(csSerial), crc, _tstoi(csLevel), nID);
			
			// store license data

			LICENSEDATA ld;
			ld.serial = csSerial;
			ld.name = _T("");
			ld.version = _T("");
			ld.code = crc;
			ld.filename = csFileName;
			m_licenses[nID] = ld;
		}
		else
		{
			AfxMessageBox(_T("Can't extract Product ID from file"), MB_ICONERROR);
		}
	}

	buf_filename.ReleaseBuffer();
	delete pOpenFileDlg;

	// Show license documents
	GenerateLicenseReports();

}

int CProgramsDlg::getProductCodeDP3(CString filename)
{
	CFile cfFile;
	BYTE buf[4];
	int nBytes = 0;

	if(cfFile.Open(filename, CFile::modeRead) == 0)
		return -1;

	cfFile.Seek(/*12*/1044, CFile::begin);	//programinfo b�rjat p� adress 0x400, l�ser endast ut produktkoden i den h�r funktionen
	nBytes = cfFile.Read(buf,4);

	cfFile.Close();

	if(nBytes != 4)
		return -1;

	int result = 0;
	for (int n = 3; n >= 0; n--)
		result = (result << 8) + buf[ n ];

	return result;
}

//Anpassat f�r dp3
bool CProgramsDlg::copyFileDP3(CString dest, CString org, CString filename, int m_nSerial, int m_nCode, int m_nLevel, int m_nProd)
{
	bool bRet = true;
	CString csTmpFN, csDate,csMsg;
char *szFileBuf;

	csDate = getDateTime();
	csDate.Remove('-');
	csDate.Remove(':');
	csDate.Remove(' ');
	csTmpFN = getMyDocumentsDir() + _T("\\");
	csTmpFN += filename;
	csTmpFN += csDate;

	//kopiera till tempor�r fil, l�gg p� tidsst�mpel till filnamnet f�r att g�ra unikt
	if(CopyFile(org,csTmpFN, FALSE) == 0)
	{
		csMsg.Format(_T("Failed to copy file\r\n%s\r\nto\r\n%s"),org,csTmpFN);
		AfxMessageBox(csMsg);
		return false;
	}

	//modifiera filen med lic kod
	CFile fh;
	if(!fh.Open(csTmpFN, CFile::modeReadWrite))
	{
		csMsg.Format(_T("Failed to open file\r\n%s"),csTmpFN);
		AfxMessageBox(csMsg);
		return false;
	}

	ULONG dwFilesize = fh.GetLength();
	szFileBuf = (char *)malloc(dwFilesize+1);
	if(szFileBuf == NULL )	// check that the malloc was sucessfull.
	{
		csMsg.Format(_T("Failed to allocate buffer"));
		AfxMessageBox(csMsg);
		fh.Close();
		return false;
	}

	// read the file into a temporar buffer
	fh.Read(szFileBuf, dwFilesize);

	// fix the code into the buffer.
	if(m_nSerial != -1 && m_nCode != -1)
	{
		m_nLevel--;	//m�ste minska med 1 annars blir checkcode fel

		if(CheckCode(m_nProd, m_nSerial, m_nLevel, m_nCode))
		{
			CTime time = CTime::GetCurrentTime();
			dp3_prg_data_type dp3_prg;
			memset(&dp3_prg, 0, sizeof(dp3_prg_data_type));
			memcpy(&dp3_prg, szFileBuf+1024, sizeof(dp3_prg_data_type));	//Vill l�sa in fr�n adress 0x400

			dp3_prg.product = dp3_prg.product + (m_nLevel << 28);	// put the level into the productcode
			dp3_prg.lic = m_nLevel+1;
			dp3_prg.snr = m_nSerial;
			
			dp3_prg.year = time.GetYear();
			dp3_prg.month = time.GetMonth();
			dp3_prg.day = time.GetDay();
			dp3_prg.hours = time.GetHour();
			dp3_prg.min = time.GetMinute();
			dp3_prg.sec = time.GetSecond();

			dp3_prg.crc = crc_16(0,(unsigned char*)&dp3_prg, sizeof(dp3_prg_data_type)-4);	//ta ej med crc i structen f�r att r�kna ut nya crc

			memcpy(szFileBuf+1024, &dp3_prg, sizeof(dp3_prg_data_type));	//spara structen p� adress 0x400
		}
	}
	else
	{
		csMsg.Format(_T("Wrong ID"));
		AfxMessageBox(csMsg);
		fh.Close();
		free(szFileBuf);
		return false;
	}

	fh.SeekToBegin();
	fh.Write(szFileBuf, dwFilesize);
	fh.Flush();
	fh.Close();
	free(szFileBuf);


	//flytta till dp2, �ndra filnamnet
	dest += filename;
	if(MoveFileEx(csTmpFN,dest,MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING) == 0)
	{
		csMsg.Format(_T("Failed to copy file to DPII"));
		AfxMessageBox(csMsg);
		return false;
	}

	return bRet;
}