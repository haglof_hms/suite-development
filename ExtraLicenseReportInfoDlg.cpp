// ExtraLicenseReportInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ExtraLicenseReportInfoDlg.h"


// CExtraLicenseReportInfoDlg dialog

IMPLEMENT_DYNAMIC(CExtraLicenseReportInfoDlg, CDialog)

CExtraLicenseReportInfoDlg::CExtraLicenseReportInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CExtraLicenseReportInfoDlg::IDD, pParent)
	, m_csName(_T(""))
	, m_csVersion(_T(""))
	, m_csFile(_T(""))
{

}

CExtraLicenseReportInfoDlg::~CExtraLicenseReportInfoDlg()
{
}

void CExtraLicenseReportInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_wndEditName);
	DDX_Control(pDX, IDC_EDIT2, m_wndEditVersion);
	DDX_Control(pDX, IDC_STATIC_FN, m_wndLblFileName);
}


BEGIN_MESSAGE_MAP(CExtraLicenseReportInfoDlg, CDialog)
END_MESSAGE_MAP()


// CExtraLicenseReportInfoDlg message handlers

BOOL CExtraLicenseReportInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_wndEditName.SetWindowTextW(m_csName);
	m_wndEditVersion.SetWindowTextW(m_csVersion);
	m_wndLblFileName.SetWindowTextW(m_csFile);

	// TODO:  Add extra initialization here
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CExtraLicenseReportInfoDlg::OnOK()
{
	// TODO: Add your specialized code here and/or call the base class
	UpdateData(TRUE);

	m_wndEditName.GetWindowTextW(m_csName);
	m_wndEditVersion.GetWindowTextW(m_csVersion);

	CDialog::OnOK();
}
