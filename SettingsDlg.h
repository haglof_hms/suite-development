#pragma once
#include "afxwin.h"
#include "SampleSuiteForms.h"


#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CSettingsFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CSettingsFrame)

	BOOL m_bOnce;
public:
	CSettingsFrame();

private:

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSettingsFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSettingsFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
	//{{AFX_MSG(CSettingsFrame)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
protected:
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
};


// CSettingsDlg form view

class CSettingsDlg : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CSettingsDlg)

protected:
	CSettingsDlg();           // protected constructor used by dynamic creation
	virtual ~CSettingsDlg();

	CMDISampleSuiteDoc* pDoc;

	CXTPPropertyGridItem* m_pMedia;
	CXTPPropertyGridItem* m_pItemMedia;

	CXTPPropertyGridItem* m_pItemServer;
	CXTPPropertyGridItem* m_pItemPathWeb;
	CXTPPropertyGridItem* m_pItemPathLocal;
	CXTPPropertyGridItem* m_pItemLogin;
	CXTPPropertyGridItem* m_pItemPass;

public:
	enum { IDD = IDD_SETTINGS };
	//{{AFX_DATA(CPropertyGridDlg)
	CStatic m_wndPlaceHolder;
	//}}AFX_DATA
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();

protected:
	CXTPPropertyGrid m_wndPropertyGrid;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg LRESULT OnValueChanged(WPARAM wParam, LPARAM lParam);
};


