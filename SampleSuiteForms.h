#ifndef _SampleSuiteFORMS_H_
#define _SampleSuiteFORMS_H_

#include "stdafx.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMDISampleSuiteDoc : public CDocument
{
protected: // create from serialization only
	CMDISampleSuiteDoc();
	DECLARE_DYNCREATE(CMDISampleSuiteDoc)

// Attributes
public:
	int m_nType;	// 0 = DigitechPro, 1 = Mantax computer, 2 = Digitech
	CString m_csFilepath;	// filepath
	CArray<PROGRAM, PROGRAM> m_caPrograms;
	int m_nSelProgram;
	int m_nSelLang;
	BOOL m_bNew;	// ugly hack
	BOOL m_bLock;
	BOOL m_bClick;	// stupid variable used becuase of a stupid concept

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDISampleSuiteDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMDISampleSuiteDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMDISampleSuiteDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

class CMDISampleSuiteFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDISampleSuiteFrame)
public:
	CMDISampleSuiteFrame();

// Attributes
public:
	CXTPDockingPaneManager m_paneManager;
	CXTPPropertyGrid m_wndPropertyGrid;

// Operations
public:
	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDISampleSuiteFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMDISampleSuiteFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	//{{AFX_MSG(CMDISampleSuiteFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg LRESULT OnDockingPaneNotify(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_CHILDFRM_H__AC306270_1FD9_4D1A_8302_E5F29AC1DB9D__INCLUDED_)
