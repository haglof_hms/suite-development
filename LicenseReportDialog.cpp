// LicenseReportDialog.cpp : implementation file
//

#include "stdafx.h"
#include "LicenseReportDialog.h"


// CLicenseReportDialog dialog

IMPLEMENT_DYNAMIC(CLicenseReportDialog, CDialog)

CLicenseReportDialog::CLicenseReportDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CLicenseReportDialog::IDD, pParent)
{

}

CLicenseReportDialog::~CLicenseReportDialog()
{
}

void CLicenseReportDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO2, m_technician);
}


BEGIN_MESSAGE_MAP(CLicenseReportDialog, CDialog)
END_MESSAGE_MAP()

BOOL CLicenseReportDialog::OnInitDialog()
{
	if( !CDialog::OnInitDialog() )
		return FALSE;

	m_technician.AddString(_T("KL"));
	m_technician.AddString(_T("ON"));
	m_technician.AddString(_T("EH"));
	m_technician.AddString(_T("F�"));

	return TRUE;
}

void CLicenseReportDialog::OnOK()
{
	if( m_technician.GetCurSel() < 0 )
		return;

	m_technician.GetWindowText(csTech);

	CDialog::OnOK();
}
