#pragma once

#include "Resource.h"
#include "SampleSuiteForms.h"
#include "MyLocale.h"
#include "atltime.h"

typedef struct _tagFILES
{
	CString csPath;
	CString csFilename;
	BOOL bPacked;
} FILES;

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CProgramFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CProgramFrame)

	BOOL m_bOnce;
public:
	CProgramFrame();
	CXTPToolBar m_wndToolBar;

// Attributes
public:
//	CXTPDockingPaneManager m_paneManager;
//	CXTPPropertyGrid m_wndPropertyGrid;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProgramFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CProgramFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	void SetButtonEnabled(UINT nID, BOOL bEnabled);

// Generated message map functions
	//{{AFX_MSG(CProgramFrame)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
protected:
	LRESULT OnMsgSuite(WPARAM wParm, LPARAM lParm);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnClose();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
};


// CProgramDlg form view

class CProgramDlg : public CXTResizeFormView  //CFormView
{
	DECLARE_DYNCREATE(CProgramDlg)

protected:
	CProgramDlg();           // protected constructor used by dynamic creation
	virtual ~CProgramDlg();

	CMDISampleSuiteDoc* pDoc;

public:
	CXTPReportControl* m_pwndReport;
	CXTPReportControl m_wndReport;

private:
	CLocale m_cLocale;
	CArray<FILES, FILES> m_caFiles;
	BOOL m_bNew;	// new package
	COleDropTarget m_dropTarget;
	int m_nSelProgram;
	int m_nSelLang;
	
	BOOL m_bDontBother;	// used to not let OnSetFocus() mess upp things...

	CXTMaskEdit m_edArtNum;

	BOOL m_bIsFocus;
public:
	enum { IDD = IDD_PROGRAM };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSave();
	afx_msg void OnDelete();

protected:
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnInitialUpdate();
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg int OpenPackage();
	afx_msg void OnReportSelChanged(NMHDR * pNotifyStruct, LRESULT * result);
	
	void OnReportFilebutton(NMHDR*  pNotifyStruct, LRESULT* /*result*/);
	int CreatePackageXML();

	DECLARE_MESSAGE_MAP()
public:
	// Programname
	CString m_csName;
	// ProgramID
	CString m_csID;
	// Programtype (0 = DigitechPro, 1 = Mantax Computer, 2 = PocketPC, 3 = WinCE, 4 = Win32)
	int m_nType;
	// Programversion
	CString m_csVersion;
	float m_fVersion;
	// Programlanguage
	int m_nLanguage;
	CString m_csLang;
	// Programdescription
	CString m_csDescription;
	// Artikelnummer (Navision)
	CString m_csArtNum;

	BOOL m_bNeedLicense;
	BOOL m_bInvisible;
	CTime m_ctDate;
	CString m_csDate;
};
