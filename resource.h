//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SampleSuite.rc
//
#define IDR_PROGRAM                     420
#define IDD_PROGRAM                     420
#define IDR_PROGRAMS                    421
#define IDD_PROGRAMS                    421
#define IDR_SETTINGS                    422
#define IDD_SETTINGS                    422
#define IDC_EDIT_NAME                   5002
#define IDC_COMBO_LANGUAGE              5003
#define IDC_EDIT_VERSION                5004
#define IDC_EDIT_ID                     5005
#define IDC_COMBO_TYPE                  5006
#define IDC_STATIC_NAME                 5007
#define IDC_STATIC_ID                   5008
#define IDC_STATIC_TYPE                 5009
#define IDC_STATIC_VERSION              5010
#define IDC_STATIC_LANGUAGE             5011
#define IDC_STATIC_LANGUAGE2            5012
#define IDC_PROPERTY_GRID               7000
#define IDC_STATIC_LICENSE_DESCRIPTION  7000
#define IDC_STATIC_PROGRAMS_KEYS        7002
#define IDR_TOOLBAR_PROGRAM             7002
#define IDC_STATIC_LICENSE_KEYS         7003
#define IDC_STATIC_FILES                7003
#define IDC_LIST_PROGRAMS_PROG          7007
#define IDC_STATIC_PROGRAMS_DESC1       7012
#define IDC_STATIC_PROGRAMS_DESC2       7013
#define IDC_EDIT_FILENAME               7032
#define IDC_BUTTON_FILECHOOSE           7035
#define IDD_DIALOG1                     7035
#define IDD_DIALOG2                     7036
#define IDD_DIALOG3                     7037
#define IDD_EXTRA_REPORT_INFO           7037
#define IDC_PLACEHOLDER                 7040
#define IDC_EDIT_DESCRIPTION            7052
#define IDC_STATIC_DESCRIPTION          7053
#define IDC_CHECK_NEEDLICENSE           7055
#define IDC_DATETIMEPICKER1             7056
#define IDC_STATIC_DATE                 7057
#define IDC_EDIT1                       7058
#define IDC_COMBO1                      7059
#define IDC_CHECK1                      7060
#define IDC_CHECK_INVISIBLE             7060
#define IDC_COMBO2                      7061
#define IDC_EDIT2                       7062
#define IDC_STATIC_FN                   7063
#define IDC_EDIT_ARTNUM                 7064
#define IDR_TOOLBAR_PROGRAMS            8001
#define ID_BUTTON_PROGRAM_OPEN          32771
#define ID_BUTTON_PROGRAM_DOWNLOAD      32772
#define ID_BUTTON_PROGRAM_PACK          32773
#define ID_BUTTON_PROGRAM_ADD           32774
#define ID_BUTTON_PROGRAM_DEL           32775
#define ID_BUTTON_PROGRAM_SELECT_FILES  32776
#define ID_BUTTON_PROGRAM_SEND_DP2      32777
#define ID_BUTTON_MANUAL                32784
#define ID_BUTTON_PROGRAM_MANUAL        32784
#define ID_BUTTON_PROGRAM_REFRESH       32785
#define ID_BUTTON_PROGRAM_SEND_DP3      32788
#define IDC_STATIC_RECEIVE_FOLDER       65535

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        7038
#define _APS_NEXT_COMMAND_VALUE         32789
#define _APS_NEXT_CONTROL_VALUE         7065
#define _APS_NEXT_SYMED_VALUE           7000
#endif
#endif
