#pragma once
#include "afxwin.h"


// CSerialDlg dialog

class CSerialDlg : public CDialog
{
	DECLARE_DYNAMIC(CSerialDlg)

public:
	CSerialDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSerialDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_csSerial;
	CComboBox m_cbLevel;
	CString m_csLevel;
	virtual BOOL OnInitDialog();
protected:
	virtual void OnOK();
};
