unsigned short crc16_typ2(unsigned char *z,int n);
unsigned  short crc16_typ1(unsigned char *z,int n);

struct prg_struct_type
{
	unsigned int mark;
	unsigned int len;
	unsigned int start;
	unsigned int productcode;
	unsigned int licenstart;
	unsigned int licensend;
	unsigned int checksum_;
};

struct dp3_prg_data_type
{
unsigned int mark;  
unsigned int lic;
unsigned int snr;
unsigned int len;
unsigned int start;
unsigned int product;
unsigned int year;
unsigned int month;
unsigned int day;
unsigned int hours;
unsigned int min;
unsigned int sec;
unsigned int crc;
};


BOOL CheckCode(int nProdID, int nSerial, int nLevel, int nCode);

#define U8    unsigned char
#define U16   unsigned short int 
unsigned short int crc_16(unsigned short int sum, const unsigned char *data, unsigned long size);
unsigned short int FS_CRC16_CalcBitByBit(const U8* pData, unsigned int len, U16 crc, U16 Polynom);