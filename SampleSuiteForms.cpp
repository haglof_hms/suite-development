#include "stdafx.h"
#include "SampleSuiteForms.h"
#include "SampleSuite.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMDISampleSuiteDoc

IMPLEMENT_DYNCREATE(CMDISampleSuiteDoc, CDocument)

BEGIN_MESSAGE_MAP(CMDISampleSuiteDoc, CDocument)
	//{{AFX_MSG_MAP(CMDISampleSuiteDoc)
	// NOTE - the ClassWizard will add and remove mapping macros here.
	//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDISampleSuiteDoc construction/destruction

CMDISampleSuiteDoc::CMDISampleSuiteDoc()
{
	// TODO: add one-time construction code here

}

CMDISampleSuiteDoc::~CMDISampleSuiteDoc()
{
}

BOOL CMDISampleSuiteDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	m_nType = theApp.m_nType;
	m_bNew = theApp.m_bNew;
	m_bLock = theApp.m_bLock;
	m_bClick = theApp.m_bClick;
//	m_csFilepath = theApp.m_csFilepath;

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMDISampleSuiteDoc serialization

void CMDISampleSuiteDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMDISampleSuiteDoc diagnostics

#ifdef _DEBUG
void CMDISampleSuiteDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMDISampleSuiteDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMDISampleSuiteDoc commands




/////////////////////////////////////////////////////////////////////////////
// CMDISampleSuiteFrame

IMPLEMENT_DYNCREATE(CMDISampleSuiteFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDISampleSuiteFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDISampleSuiteFrame)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
	ON_MESSAGE(XTPWM_DOCKINGPANE_NOTIFY, OnDockingPaneNotify)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDISampleSuiteFrame construction/destruction

CMDISampleSuiteFrame::CMDISampleSuiteFrame()
{
	// TODO: add member initialization code here

}

CMDISampleSuiteFrame::~CMDISampleSuiteFrame()
{
}

BOOL CMDISampleSuiteFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMDISampleSuiteFrame diagnostics

#ifdef _DEBUG
void CMDISampleSuiteFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDISampleSuiteFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMDISampleSuiteFrame message handlers

int CMDISampleSuiteFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	CenterWindow();

	UpdateWindow();

	return 0;
}

LRESULT CMDISampleSuiteFrame::OnDockingPaneNotify(WPARAM wParam, LPARAM lParam)
{
	if (wParam == XTP_DPN_SHOWWINDOW)
	{
		return TRUE;

	}
	return FALSE;
}
