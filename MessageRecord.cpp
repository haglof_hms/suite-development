// MessageRecord.cpp: implementation of the CMessageRecord class.
//
// This file is a part of the XTREME TOOLKIT PRO MFC class library.
// �1998-2005 Codejock Software, All Rights Reserved.
//
// THIS SOURCE FILE IS THE PROPERTY OF CODEJOCK SOFTWARE AND IS NOT TO BE
// RE-DISTRIBUTED BY ANY MEANS WHATSOEVER WITHOUT THE EXPRESSED WRITTEN
// CONSENT OF CODEJOCK SOFTWARE.
//
// THIS SOURCE CODE CAN ONLY BE USED UNDER THE TERMS AND CONDITIONS OUTLINED
// IN THE XTREME TOOLKIT PRO LICENSE AGREEMENT. CODEJOCK SOFTWARE GRANTS TO
// YOU (ONE SOFTWARE DEVELOPER) THE LIMITED RIGHT TO USE THIS SOFTWARE ON A
// SINGLE COMPUTER.
//
// CONTACT INFORMATION:
// support@codejock.com
// http://www.codejock.com
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "ReportSample.h"
#include "MessageRecord.h"
//#include "ReportSampleView.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


IMPLEMENT_SERIAL(CMessageRecordItemCheck, CXTPReportRecordItem, 0)
IMPLEMENT_SERIAL(CMessageRecordItemCombo, CXTPReportRecordItem, 0)
IMPLEMENT_SERIAL(CMessageRecordItemFilebox, CXTPReportRecordItem, VERSIONABLE_SCHEMA | _XTP_SCHEMA_CURRENT)

//////////////////////////////////////////////////////////////////////////
// CMessageRecordItemCheck

CMessageRecordItemCheck::CMessageRecordItemCheck(BOOL bCheck)
{
	HasCheckbox(TRUE);
	SetChecked(bCheck);
}

int CMessageRecordItemCheck::GetGroupCaptionID(CXTPReportColumn* /*pColumn*/)
{
//	return IsChecked()? IDS_GROUP_CHECKED_TRUE: IDS_GROUP_CHECKED_FALSE;
	return FALSE;
}

int CMessageRecordItemCheck::Compare(CXTPReportColumn* /*pColumn*/, CXTPReportRecordItem* pItem)
{
	return int(IsChecked()) - int(pItem->IsChecked());
}

//////////////////////////////////////////////////////////////////////
// CMessageRecord class

IMPLEMENT_SERIAL(CMessageRecord, CXTPReportRecord, 0)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMessageRecord::CMessageRecord()
{
	CreateItems();
}

CMessageRecord::CMessageRecord(
		BOOL bLocal,
		int nProgID,
		CString strName,
		CString strVersion,
//		CString strNewVersion,
		CString strLanguage,
		CString strTLA
//		int nType
		)
{
	m_nProgID = nProgID;

	CXTPReportRecordItem *pItem = new CXTPReportRecordItem; pItem->HasCheckbox(TRUE);
	AddItem(pItem);
	AddItem(new CXTPReportRecordItemText(strName));
	AddItem(new CXTPReportRecordItemText(strVersion));
//	AddItem(new CXTPReportRecordItemText(strNewVersion));
	AddItem(new CXTPReportRecordItemText(strLanguage));
	AddItem(new CXTPReportRecordItemText(strTLA));
//	AddItem(new CXTPReportRecordItemText(nType));

	CString csBuf;
	csBuf.Format(_T("%d"), nProgID);
	AddItem(new CXTPReportRecordItemText(csBuf));
}

void CMessageRecord::CreateItems()
{
	// Initialize record items with empty values
	AddItem(new CXTPReportRecordItemText(_T("")));	// name
	AddItem(new CXTPReportRecordItemText(_T("")));	// version
//	AddItem(new CXTPReportRecordItemText(_T("")));
	AddItem(new CXTPReportRecordItemText(_T("")));	// lang
	AddItem(new CXTPReportRecordItemText(_T("")));	// tla
//	AddItem(new CXTPReportRecordItemText(_T("")));
	AddItem(new CXTPReportRecordItemText(_T("")));	// progid
}

CMessageRecord::~CMessageRecord()
{

}

BOOL CMessageRecord::SetRead()
{
	return TRUE;
}

void CMessageRecord::GetItemMetrics(XTP_REPORTRECORDITEM_DRAWARGS* pDrawArgs, XTP_REPORTRECORDITEM_METRICS* pItemMetrics)
{
//	if (m_pItemIcon && !m_pItemIcon->m_bRead && !pDrawArgs->pItem->IsPreviewItem())
//		pItemMetrics->pFont = &pDrawArgs->pControl->GetPaintManager()->m_fontBoldText;
/*
	CReportSampleView* pView = DYNAMIC_DOWNCAST(CReportSampleView, pDrawArgs->pControl->GetParent());

	// If automatic formatting option is enabled, sample code below will be executed.
	// There you can see an example of "late" customization for colors, fonts, etc.
	if (pView && pView->m_bAutomaticFormating)
	{
		if ((pDrawArgs->pRow->GetIndex() % 2) && !pDrawArgs->pItem->IsPreviewItem())
		{
			pItemMetrics->clrBackground = RGB(245, 245, 245);
		}
		if (pDrawArgs->pItem->GetCaption(pDrawArgs->pColumn).Find(_T("Undeliverable")) >= 0)
		{
			pItemMetrics->clrForeground = RGB(0xFF, 0, 0);

		}
	}
*/
}

void CMessageRecord::DoPropExchange(CXTPPropExchange* pPX)
{
	CXTPReportRecord::DoPropExchange(pPX);

	if (pPX->IsLoading())
	{
/*		// 1 - m_pItemIcon = (CMessageRecordItemIcon*)AddItem(new CMessageRecordItemIcon(TRUE));
		ASSERT_KINDOF(CMessageRecordItemIcon, GetItem(1));
		m_pItemIcon = DYNAMIC_DOWNCAST(CMessageRecordItemIcon, GetItem(1));
		ASSERT(m_pItemIcon);
		
		// 5 - m_pItemReceived = (CMessageRecordItemDate*)AddItem(new CMessageRecordItemDate(dtNow));
		ASSERT_KINDOF(CMessageRecordItemDate, GetItem(5));
		m_pItemReceived = DYNAMIC_DOWNCAST(CMessageRecordItemDate, GetItem(5));
		ASSERT(m_pItemReceived);
		
		// 6 - m_pItemSize = AddItem(new CXTPReportRecordItemNumber(0));
		ASSERT_KINDOF(CXTPReportRecordItemNumber, GetItem(6));
		m_pItemSize = DYNAMIC_DOWNCAST(CXTPReportRecordItemNumber, GetItem(6));
		ASSERT(m_pItemSize);		
*/
	}
}

//////////////////////////////////////////////////////////////////////
// CMessageRecord class

IMPLEMENT_SERIAL(CKeyRecord, CXTPReportRecord, 0)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CKeyRecord::CKeyRecord()
{
	CreateItems();
}

CKeyRecord::CKeyRecord(CString csFilename, int nType)
{
	CXTPReportRecordItem* pItem = AddItem(new /*CXTPReportRecordItemText*/CMessageRecordItemFilebox(csFilename));
	pItem->SetItemData(0);

	pItem = AddItem(new CMessageRecordItemCombo(nType));
	pItem->SetItemData(1);
}

void CKeyRecord::CreateItems()
{
	// Initialize record items with empty values
}

CKeyRecord::~CKeyRecord()
{
}

//////////////////////////////////////////////////////////////////////////
// CMessageRecordItemCombo

CMessageRecordItemCombo::CMessageRecordItemCombo(int nLevel)
{
	m_nValue = nLevel;

	GetEditOptions(NULL)->AddConstraint(_T(""), 0);
	GetEditOptions(NULL)->AddConstraint(_T("DP"), 1);
	GetEditOptions(NULL)->AddConstraint(_T("DPDOC"), 2);
	GetEditOptions(NULL)->AddConstraint(_T("GENERIC"), 3);
	GetEditOptions(NULL)->m_bConstraintEdit = TRUE;
	GetEditOptions(NULL)->AddComboButton();
}

CString CMessageRecordItemCombo::GetCaption(CXTPReportColumn* /*pColumn*/)
{
	CXTPReportRecordItemConstraint* pConstraint = GetEditOptions(NULL)->FindConstraint(m_nValue);
	ASSERT(pConstraint);
	return pConstraint->m_strConstraint;
}

void CMessageRecordItemCombo::OnConstraintChanged(XTP_REPORTRECORDITEM_ARGS*, CXTPReportRecordItemConstraint* pConstraint)
{
	m_nValue = (int)pConstraint->m_dwData;
}


//////////////////////////////////////////////////////////////////////////
// CMessageRecordItemFilebox

CMessageRecordItemFilebox::CMessageRecordItemFilebox(LPCTSTR szText)
	: CXTPReportRecordItem(), m_strText(szText)
{
	GetEditOptions(NULL)->AddExpandButton();
}

void CMessageRecordItemFilebox::OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
{
	SetValue(szText);
}

CString CMessageRecordItemFilebox::GetCaption(CXTPReportColumn* /*pColumn*/)
{
	if (!m_strCaption.IsEmpty())
		return m_strCaption;

	if (m_strFormatString == _T("%s"))
		return m_strText;

	CString strCaption;
	strCaption.Format(m_strFormatString, (LPCTSTR)m_strText);
	return strCaption;
}


void CMessageRecordItemFilebox::DoPropExchange(CXTPPropExchange* pPX)
{
	CXTPReportRecordItem::DoPropExchange(pPX);
	PX_String(pPX, _T("Text"), m_strText, _T(""));
}

void CMessageRecordItemFilebox::OnInplaceButtonDown(CXTPReportInplaceButton* pButton)
{
	CXTPReportRecordItem::OnInplaceButtonDown(pButton);
}
