// SampleSuite.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "SampleSuiteForms.h"
#include "SettingsDlg.h"
#include "ProgramsDlg.h"
#include "ProgramDlg.h"

/////////////////////////////////////////////////////////////////////////////
// Initialization of MFC Extension DLL

#include "afxdllx.h"    // standard MFC Extension DLL routines


std::vector<HINSTANCE> m_vecHInstTable;
RLFReader* g_pXML;
CZipArchive* g_pZIP;
static CWinApp* pApp = AfxGetApp();
HINSTANCE hInst = NULL;

static AFX_EXTENSION_MODULE SampleSuiteDLL = { NULL, NULL };

CSampleSuite::CSampleSuite()
{
	// fetch some values from the registry
	m_csDownloadPath = _T("C:\\");
	m_csFilePath = _T("C:\\");
	m_csWebPath = regGetStr(REG_ROOT, _T("\\HMS_Development\\Settings"), _T("WebPath"), _T("/DP/"));
	m_csWebServer = regGetStr(REG_ROOT, _T("\\HMS_Development\\Settings"), _T("WebServer"), _T("download.haglof.se"));
	m_csWebLogin = regGetStr(REG_ROOT, _T("\\HMS_Development\\Settings"), _T("WebLogin"), _T(""));
	m_csWebPass = regGetStr(REG_ROOT, _T("\\HMS_Development\\Settings"), _T("WebPass"), _T(""));

	m_csLocalPath = regGetStr(REG_ROOT, _T("HMS_Development\\Settings"), _T("LocalPath"), _T("C:\\"));

	m_bWriteAllowed = regGetInt(REG_ROOT, _T("HMS_Development\\Settings"), _T("WriteAllowed"), 0);

	m_bNew = FALSE;
	m_bLock = FALSE;

	TCHAR szPath[MAX_PATH];
	GetTempPath(MAX_PATH, szPath);
	m_csTemppath = szPath;
}

CSampleSuite::~CSampleSuite()
{
	// save some settings in the registry
	regSetStr(REG_ROOT, _T("\\HMS_Development\\Settings"), _T("WebPath"), m_csWebPath);
	regSetStr(REG_ROOT, _T("\\HMS_Development\\Settings"), _T("WebServer"), m_csWebServer);
	regSetStr(REG_ROOT, _T("\\HMS_Development\\Settings"), _T("WebLogin"), m_csWebLogin);
	regSetStr(REG_ROOT, _T("\\HMS_Development\\Settings"), _T("WebPass"), m_csWebPass);

	regSetStr(REG_ROOT, _T("\\HMS_Development\\Settings"), _T("LocalPath"), m_csLocalPath);
}

BOOL CSampleSuite::CheckLicense(CString csLicModule)
{
/*	TCHAR szBuf[MAX_PATH], szFilename[MAX_PATH];
	GetModuleFileName(hInst, szBuf, sizeof(szBuf) / sizeof(TCHAR));
	_tsplitpath(szBuf, NULL, NULL, szFilename, NULL);

	_user_msg msg(820, _T("CheckLicense"), _T("License.dll"), csLicModule, g_pXML->str(800), _T("0"), &szFilename);
	int nRet = AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&msg);

	if(_tcscmp(msg.getFunc(), _T("1")) != 0 ||
		_tcscmp(msg.getArgStr(), _T("1")) == 0 ||
		_tcscmp(msg.getFunc(), _T("CheckLicense")) == 0)	// demo
	{
		return FALSE;
	}
*/
	return TRUE;
}

CSampleSuite theApp;


extern "C" int APIENTRY DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("Development.DLL Initializing!\n");
	
		// create the XML-parser class.
		g_pXML = new RLFReader();
		g_pZIP = new CZipArchive();

		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(SampleSuiteDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("Development.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(SampleSuiteDLL);

		delete g_pZIP;
		delete g_pXML;
	}

	hInst = hInstance;

	return 1;   // ok
}

// Exported DLL initialization is run in context of running application
void DLL_BUILD InitSuite(CStringArray *user_modules, vecINDEX_TABLE &vecIndex, vecINFO_TABLE &vecInfo)
{
	// create a new CDynLinkLibrary for this app
	new CDynLinkLibrary(SampleSuiteDLL);

	CString csModuleFN = getModuleFN(hInst);
	m_vecHInstTable.clear();

	// Setup the language filename
	CString csLangFN;
	csLangFN.Format(_T("%s%s"), getLanguageDir(), PROGRAM_NAME);

	CWinApp* pApp = AfxGetApp();

	// Program
	pApp->AddDocTemplate(new CMultiDocTemplate(IDR_PROGRAM, 
			RUNTIME_CLASS(CMDISampleSuiteDoc),
			RUNTIME_CLASS(CProgramFrame),
			RUNTIME_CLASS(CProgramDlg)));
	vecIndex.push_back(INDEX_TABLE(IDR_PROGRAM, csModuleFN, csLangFN, TRUE));

	// Programs
	pApp->AddDocTemplate(new CMultiDocTemplate(IDR_PROGRAMS, 
			RUNTIME_CLASS(CMDISampleSuiteDoc),
			RUNTIME_CLASS(CProgramsFrame),
			RUNTIME_CLASS(CProgramsDlg)));
	vecIndex.push_back(INDEX_TABLE(IDR_PROGRAMS, csModuleFN, csLangFN, TRUE));

	// Settings
	pApp->AddDocTemplate(new CMultiDocTemplate(IDR_SETTINGS, 
			RUNTIME_CLASS(CMDISampleSuiteDoc),
			RUNTIME_CLASS(CSettingsFrame),
			RUNTIME_CLASS(CSettingsDlg)));
	vecIndex.push_back(INDEX_TABLE(IDR_SETTINGS, csModuleFN, csLangFN, TRUE));


	// Get version information; 060803 p�d
	CString csVersion, csCopyright, csCompany;

	csVersion	= getVersionInfo(hInst, VER_NUMBER);
	csCopyright	= getVersionInfo(hInst, VER_COPYRIGHT);
	csCompany	= getVersionInfo(hInst, VER_COMPANY);

#ifdef UNICODE
	vecInfo.push_back(INFO_TABLE(-999,
		1, //Set to 1 to indicate a SUITE; 2 indicates a  User Module,
		csLangFN,
		csVersion,
		csCopyright,
		csCompany ));
#else
	vecInfo.push_back(INFO_TABLE(-999,
		1, //Set to 1 to indicate a SUITE; 2 indicates a  User Module,
		csLangFN.GetBuffer(0),
		csVersion.GetBuffer(0),
		csCopyright.GetBuffer(0),
		csCompany.GetBuffer(0)));
#endif

	/* *****************************************************************************
		Load user module(s), specified in the ShellTree data file for this SUITE
	****************************************************************************** */
	typedef CRuntimeClass *(*Func)(CWinApp *, LPCTSTR suite, vecINDEX_TABLE &, vecINFO_TABLE &);
	Func proc;
	// Try to get modules connected to this Suite; 051129 p�d
	if (user_modules->GetCount() > 0)
	{
		for (int i = 0;i < user_modules->GetCount();i++)
		{
			CString sPath;
			sPath.Format(_T("%s%s"), getModulesDir(), user_modules->GetAt(i));
			// Check if the file exists, if not tell USER; 051213 p�d
			if (fileExists(sPath))
			{
				HINSTANCE hInst = AfxLoadLibrary(sPath);
				if (hInst != NULL)
				{
					m_vecHInstTable.push_back(hInst);
					proc = (Func)GetProcAddress((HMODULE)m_vecHInstTable[m_vecHInstTable.size() - 1], "InitModule" );
					if (proc != NULL)
					{
						// call the function
						proc(pApp, csModuleFN, vecIndex, vecInfo);
					}	// if (proc != NULL)
				}	// if (hInst != NULL)

			}	// if (fileExists(sPath))
			else
			{
				// Set Messages from language file; 051213 p�d
				::MessageBox(0,_T("File doesn't exist\n" + sPath),_T("Error"),MB_OK);
			}
		}	// for (int i = 0;i < m_sarrModules.GetCount();i++)
	}	// if (m_sarrModules.GetCount() > 0)
}

void DLL_BUILD OpenSuite(int idx, LPCTSTR func, CWnd *wnd, vecINDEX_TABLE &vecIndex, int *ret)
{
	CDocTemplate *pTemplate;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sLangFN;
	CString sVecIndexTableModuleFN;
	CString sCaption;
	int nTableIndex;
	int nType=0;
	BOOL bFound = FALSE, bIsOneInst = FALSE;
	CString csPath;

	CWinApp* pApp = AfxGetApp();
	ASSERT(pApp != NULL);

	// Get path and filename of this SUITE; 051213 p�d
	sModuleFN = getModuleFN(hInst);

	// Find template name for idx value; 051124 p�d
	if (vecIndex.size() > 0)
	{
		for (UINT i = 0;i < vecIndex.size();i++)
		{
			nTableIndex = vecIndex[i].nTableIndex;
			sVecIndexTableModuleFN = vecIndex[i].szSuite;

			if (nTableIndex == idx && sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
			{
				// Get the stringtable resource, matching the TableIndex
				// This string is compared to the title of the document; 051212 p�d
				sResStr.LoadString(nTableIndex);

				// Get filename including searchpath to THIS SUITE, as set in
				// the table index vector, for suites and module(s); 051213 p�d
				sVecIndexTableModuleFN = vecIndex[i].szSuite;

				// Need to setup the Actual filename here, because we need to 
				// get the Language set in registry, on Openning a View; 051214 p�d
				sLangFN = vecIndex[i].szLanguageFN;

				bIsOneInst = vecIndex[i].bOneInstance;

				if (g_pXML->Load(sLangFN))
				{
					sCaption = g_pXML->str(nTableIndex);
				}
				else
				{
					AfxMessageBox(_T("Could not open languagefile!"), MB_ICONERROR);
				}

				// Check if the document or module is in this SUITE; 051213 p�d
				if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
				{
					POSITION pos = pApp->GetFirstDocTemplatePosition();
					while(pos != NULL)
					{
						pTemplate = pApp->GetNextDocTemplate(pos);
						pTemplate->GetDocString(sDocName, CDocTemplate::docName);
						ASSERT(pTemplate != NULL);
						// Need to add a linefeed, infront of the docName.
						// This is because, for some reason, the document title,
						// set in resource, must have a linefeed.
						// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
						sDocName = '\n' + sDocName;

						if (pTemplate && sDocName.Compare(sResStr) == 0)
						{
							if(bIsOneInst == TRUE)
							{
								// Find the CDocument for this tamplate, and set title.
								// Title is set in Languagefile; OBS! The nTableIndex
								// matches the string id in the languagefile; 051129 p�d
								POSITION posDOC = pTemplate->GetFirstDocPosition();
								while(posDOC != NULL)
								{
									CMDISampleSuiteDoc* pDocument = (CMDISampleSuiteDoc*)pTemplate->GetNextDoc(posDOC);
									POSITION pos = pDocument->GetFirstViewPosition();
									if(pos != NULL && pDocument->m_nType == nType)
									{
										CView* pView = pDocument->GetNextView(pos);
										pView->GetParent()->BringWindowToTop();
										pView->GetParent()->SetFocus();
										posDOC = (POSITION)1;
										break;
									}
								}

								if(posDOC == NULL)
								{
									CMDISampleSuiteDoc* pDocument = (CMDISampleSuiteDoc*)pTemplate->OpenDocumentFile(NULL);
									if(pDocument == NULL) break;

									CString sDocTitle;
									sDocTitle.Format(_T("%s"), sCaption);
									pDocument->SetTitle(sDocTitle);
								}
							}
							else
							{
								CMDISampleSuiteDoc* pDocument = (CMDISampleSuiteDoc*)pTemplate->OpenDocumentFile(NULL);
								if(pDocument == NULL) break;
								
								CString sDocTitle;
								sDocTitle.Format(_T("%s"), sCaption);
								pDocument->SetTitle(sDocTitle);
							}

							break;
						}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
					}	// while(pos != NULL)
				} // if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
				*ret = 1;
			}	// if (nTableIndex == idx)
		}	// for (UINT i = 0;i < vecIndex.size();i++)
	}	// if (vecIndex.size() > 0)
	else
	{
		*ret = 0;
	}
}

// Use this function, when calling from inside another Suite/User module; 060619 p�d
void DLL_BUILD OpenSuiteEx(_user_msg *msg,CWnd *wnd,vecINDEX_TABLE &vecIndex,int *ret)
{
	CDocTemplate *pTemplate;
	CString sFuncStr;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sVecIndexTableModuleFN;
	CString sLangFN;
	CString sCaption;
	CString sLangSet;
	CString sFileToOpen;
	int nTableIndex;
	int nDocCounter;
	BOOL bFound = FALSE;
	BOOL bIsOneInst;

	ASSERT(pApp != NULL);

	// Get path and filename of this SUITE; 051213 p�d
	sModuleFN = getModuleFN(hInst);

	// Find template name for idx value; 051124 p�d
	if (vecIndex.size() == 0)
		return;

	// Get language abbrevatein set, from registry; 060111 p�d
	sLangSet = getLangSet();

	for (UINT i = 0;i < vecIndex.size();i++)
	{
		nTableIndex = vecIndex[i].nTableIndex;
		if (nTableIndex == msg->getIndex())
		{
			// Get filename including searchpath to THIS SUITE, as set in
			// the table index vector, for suites and module(s); 051213 p�d
			sVecIndexTableModuleFN = vecIndex[i].szSuite;
			// Need to setup the Actual filename here, because we need to 
			// get the Language set in registry, on Openning a View; 051214 p�d
			sLangFN = vecIndex[i].szLanguageFN;

			sFileToOpen = msg->getFileName();

			bFound = TRUE;
			bIsOneInst = vecIndex[i].bOneInstance;
			break;
		}	// if (nTableIndex == idx)
	}	// for (UINT i = 0;i < vecIndex.size();i++)
	
	if (bFound)
	{
		// Get the stringtable resource, matching the TableIndex
		// This string is compared to the title of the document; 051212 p�d
		sResStr.LoadString(nTableIndex);

		RLFReader *xml = new RLFReader();
		if (xml->Load(sLangFN))
		{
			sCaption = xml->str(nTableIndex);
		}
		delete xml;

		// Check if the document or module is in this SUITE; 051213 p�d
		if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		{
			POSITION pos = pApp->GetFirstDocTemplatePosition();
			while(pos != NULL)
			{
				pTemplate = pApp->GetNextDocTemplate(pos);
				pTemplate->GetDocString(sDocName, CDocTemplate::docName);
				ASSERT(pTemplate != NULL);
				// Need to add a linefeed, infront of the docName.
				// This is because, for some reason, the document title,
				// set in resource, must have a linefeed.
				// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
				sDocName = '\n' + sDocName;

				if (pTemplate && sDocName.Compare(sResStr) == 0)
				{
				
					pTemplate->OpenDocumentFile(NULL);
					// Find the CDocument for this tamplate, and set title.
					// Title is set in Languagefile; OBS! The nTableIndex
					// matches the string id in the languagefile; 051129 p�d
					POSITION posDOC = pTemplate->GetFirstDocPosition();
					nDocCounter = 1;

					while (posDOC != NULL)
					{
						CDocument* pDocument = pTemplate->GetNextDoc(posDOC);

						// Set the caption of the document. Can be a resource string,
						// a string set in the language xml-file etc.
						CString sDocTitle;
//						sDocTitle.Format("%s (%d)",sCaption,nDocCounter);
						sDocTitle.Format(_T("%s - [%s]"), sCaption, sFileToOpen);
						pDocument->SetTitle(sDocTitle);
						nDocCounter++;
					}

					break;
				}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
			}	// while(pos != NULL)
		} // if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		*ret = 1;
	}	// if (bFound)
	else
	{
		*ret = 0;
	}

}
