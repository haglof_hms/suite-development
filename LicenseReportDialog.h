#pragma once
#include "afxwin.h"


// CLicenseReportDialog dialog

class CLicenseReportDialog : public CDialog
{
	DECLARE_DYNAMIC(CLicenseReportDialog)

public:
	CLicenseReportDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLicenseReportDialog();

	BOOL OnInitDialog();

	CString csTech;

// Dialog Data
	enum { IDD = IDD_DIALOG2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	afx_msg void OnOK();
	DECLARE_MESSAGE_MAP()
private:
	CComboBox m_technician;
};
