#pragma once
#include "afxwin.h"


// CExtraLicenseReportInfoDlg dialog

class CExtraLicenseReportInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CExtraLicenseReportInfoDlg)

public:
	CExtraLicenseReportInfoDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CExtraLicenseReportInfoDlg();

// Dialog Data
	enum { IDD = IDD_EXTRA_REPORT_INFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_csName;
	CString m_csVersion;
	CString m_csFile;
	virtual BOOL OnInitDialog();
protected:
	virtual void OnOK();
public:
	CEdit m_wndEditName;
	CEdit m_wndEditVersion;
	CStatic m_wndLblFileName;
};
